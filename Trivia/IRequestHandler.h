#pragma once
#include "RequestsStructs.h"
class IRequestHandler
{
public:
	virtual bool isRequestRelevant(RequestInfo requestInfo) = 0; //check if the request is valid in the point of the conecting
	virtual RequestResult handleRequest(RequestInfo requestInfo) = 0; //handle the request
};
