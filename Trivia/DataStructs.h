#pragma once
#include <iostream>
#include "Question.h"
typedef struct RoomData {

	int id;
	std::string name;
	int maxPlayers;
	int timePerQuestion;
	int questionCount;
	int isActive;
};

typedef struct GameData
{
	Question currentQuestion;
	int correctAnswerCount;
    int wrongAnswerCount;
	long double averangeAnswerTime;
};