#include "RequestHandlerFactory.h"
#include "LoginRequestHandler.h"
#include "MenuRequestHandler.h" 
#include "RoomAdminRequestHandler.h"
#include "RoomMemberRequestHandler.h"
#include "GameRequestHandler.h"
#include "LoggedUser.h"
#include "IDatabase.h"

/* Null, because instance will be initialized on demand. */
RequestHandlerFactory* RequestHandlerFactory::factory_instance = nullptr;

RequestHandlerFactory* RequestHandlerFactory::instance(IDatabase* dataAccess)
{
	if (!factory_instance)
		factory_instance = new RequestHandlerFactory(dataAccess);
	return factory_instance;
}

//c'tor
RequestHandlerFactory::RequestHandlerFactory(IDatabase* dataAccess) : m_StatisticsManager(dataAccess), m_gameManager(dataAccess)
{
	this->m_database = dataAccess;
	this->m_loginManager = LoginManager::instance(dataAccess);
}

//create handlers
LoginRequestHandler* RequestHandlerFactory::createLoginRequestHandler()
{
	return new LoginRequestHandler(m_database);
}
RoomAdminRequestHandler* RequestHandlerFactory::createRoomAdminRequestHandler(LoggedUser user, Room& roomData)
{
	return new RoomAdminRequestHandler(RequestHandlerFactory::factory_instance, user, roomData);
}
RoomMemberRequestHandler* RequestHandlerFactory::createRoomMemberRequestHandler(LoggedUser user, Room& roomData)
{
	return new RoomMemberRequestHandler(RequestHandlerFactory::factory_instance, user, roomData);
}
GameRequestHandler* RequestHandlerFactory::createGameRequestHandler(Game& m_game, LoggedUser m_user)
{
	return new GameRequestHandler(RequestHandlerFactory::factory_instance, m_game, m_user);
}
MenuRequestHandler* RequestHandlerFactory::createMenuRequestHandler(LoggedUser user)
{
	return new MenuRequestHandler(user.getUsername(), RequestHandlerFactory::instance(this->m_database));
}

//getters of the managaer

LoginManager* RequestHandlerFactory::getLoginManager()
{
	return this->m_loginManager;
}

StatisticsManager& RequestHandlerFactory::getStatisticsManager()
{
	return this->m_StatisticsManager;
}

RoomManager& RequestHandlerFactory::getRoomManager()
{
	return this->m_roomManager;
}

GameManager& RequestHandlerFactory::getGameManager()
{
	return this->m_gameManager;
}

