#pragma once
#include <iostream>
#include "sqlite3.h"
#include <io.h>
#include <vector>
#include <list>
#include "Question.h"
#include "ResponseStructs.h"
#include "LoggedUser.h"
class IDatabase
{
public:
	virtual ~IDatabase() = default;
	
	virtual bool open() = 0; //open the db

	//methods of get and update db
	virtual bool doesUserExist(std::string username) = 0;
	virtual bool doesPasswordMatch(std::string username, std::string password) = 0;

	virtual void addNewUser(std::string username, std::string password, std::string email,
		std::string address, std::string phoneNumber, std::string birthday) = 0;
	virtual std::list<Question> getQuestions(int numberOfQuestions) = 0;
	virtual void close() = 0;
	virtual double getPlayerAverageAnswerTime(std::string username) = 0;
	virtual int getNumOfCorrectAnswers(std::string username) = 0;
	virtual int getNumOfTotalAnswers(std::string username) = 0;
	virtual int getNumOfPlayerGames(std::string username) = 0;
	virtual std::vector<TopUser> getTopFiveScores() = 0;
	virtual void updateStatistics(GameData gameData, LoggedUser user) = 0;
};