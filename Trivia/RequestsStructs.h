#pragma once

class IRequestHandler;
#include <iostream>
#include <ctime>
#include <vector>

typedef struct SignupRequest {
	std::string username;
	std::string password;
	std::string email;
	std::string address;
	std::string phoneNumber;
	std::string birthday;
} SignupRequest;

typedef struct LoginRequest {
	std::string username;
	std::string password;
} LoginRequest;

typedef struct RequestInfo {
	uint_least8_t RequestId;
	std::time_t receivalTime;
	std::vector<unsigned char> buffer;
} RequestInfo;

typedef struct RequestResult {
	IRequestHandler* newHandler;
	std::vector<unsigned char> response;
} RequestResult;


typedef struct GetPlayersInRoomRequest {
	unsigned int roomId;
} GetPlayersInRoomRequest;


typedef struct JoinRoomRequest {
	unsigned int roomId;
} JoinRoomRequest;


typedef struct CreateRoomRequest {
	std::string roomName;
	int maxUsers;
	int questionCount;
	int answerTimeout;
} CreateRoomRequest;

typedef struct SubmitAnswerRequest {
	int answerId;
	int answerTime;
} SubmitAnswerRequest;




