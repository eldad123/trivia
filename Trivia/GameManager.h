#pragma once
#include <iostream>
#include "Room.h"
#include "Game.h"
#include "IDatabase.h"
#include <list>

class GameManager
{
public:
	GameManager(IDatabase* dataAccess);//c'tor
	Game& createGame(Room room);//creare new game
	void deleteGame(Game* game);//delete game
	Game& getGame(LoggedUser user);//get a game by user of in it

private:
	IDatabase* m_database; //database
	std::vector<Game*> m_games;//vector of all the games
};