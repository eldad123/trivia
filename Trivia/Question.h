#pragma once
#include <iostream>
#include <vector>
class Question
{
private:
	std::string m_question; //the question
	std::string correctAnswer; //the correct answer
	std::vector<std::string> m_possibleAnswers; //vector of the possible answers


public:
	Question(std::string question, std::string correctAnswer, std::string wrongAnswer1, std::string wrongAnswer2, std::string wrongAnswer3); //c'tor
	//getters
	std::string getQuestion(); 
	std::string getPossibleAnswers();
	std::vector<std::string>& getPossibleAnswersVec();
	std::string getCorrectAnswer();
	
	bool operator==(const Question& user) const;

};