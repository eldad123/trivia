#pragma once
#include <iostream>
#include <vector>
#include <ciso646>
#include "json.hpp"
#include "ResponseStructs.h"
#define SIZE_DATA_LEN 4

class JsonResponsePacketSerializer
{
public:
	//convert response to buffer

	static std::vector<unsigned char> serializeResponse(SignupResponse signup);
	static std::vector<unsigned char> serializeResponse(LoginResponse login);
	static std::vector<unsigned char> serializeResponse(ErrorResponse err);
	static std::vector<unsigned char> serializeResponse(LogoutResponse logout);
	static std::vector<unsigned char> serializeResponse(CreateRoomResponse createRoom);
	static std::vector<unsigned char> serializeResponse(JoinRoomResponse joinRoom);
	static std::vector<unsigned char> serializeResponse(GetRoomsResponse getRooms);
	static std::vector<unsigned char> serializeResponse(GetPlayersInRoomResponse getPlayers);
	static std::vector<unsigned char> serializeResponse(GetStatisticsResponse getStats);

	static std::vector<unsigned char> serializeResponse(CloseRoomResponse closeRoom);
	static std::vector<unsigned char> serializeResponse(StartGameResponse startGame);
	static std::vector<unsigned char> serializeResponse(GetRoomStateResponse gameState);
	static std::vector<unsigned char> serializeResponse(LeaveRoomResponse leaveRoom);

	static std::vector<unsigned char> serializeResponse(GetGameResultsResponse getResults);
	static std::vector<unsigned char> serializeResponse(SubmitAnswerResponse submitAnswer);
	static std::vector<unsigned char> serializeResponse(GetQuestionResponse submitAnswer);
	static std::vector<unsigned char> serializeResponse(LeaveGameResponse leaveGame);

private:
	//get all the parameters of the protocol and push it to vector
	static std::vector<unsigned char> pushProtocolParamsToVector(int code, int dataLen, std::string data);
};
