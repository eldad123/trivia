#include "Room.h"
#include <string>
#include <mutex>
#define ADMIN_LEFT_ERROR "The admin has just left the room"
#define NO_PLACE_ERROR "No more place in the room"

//c'tor
Room::Room(RoomData roomData, LoggedUser admin) : m_admin(admin)
{
	this->m_metadata = roomData;
}

//add input user to room
void Room::addUser(LoggedUser user)
{
	if (this->m_users.size() < (this->m_metadata.maxPlayers))
	{
		this->m_users.push_back(user);
	}
	else
	{
		throw std::exception(NO_PLACE_ERROR);
	}
}

//remove an input user from the room
void Room::removeUser(LoggedUser user)
{
	auto it = std::find(m_users.begin(), m_users.end(), user);

	if (it != m_users.end())
	{
		m_users.erase(it);
	}
}

//get all users that in the room
std::vector<std::string> Room::getAllUsers()
{

	std::vector<std::string> allUsernames;
	for (auto& user : this->m_users)
	{
		allUsernames.push_back(user.getUsername());
	}
	return allUsernames;
}

//get the room data
RoomData& Room::getRoomData()
{
	return this->m_metadata;
}

LoggedUser Room::getAdmin()
{
	return this->m_admin;
}
