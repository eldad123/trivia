#include "RoomMemberRequestHandler.h"
#include "JsonResponsePacketSerializer.h"

#define FAILED 0
#define SUCCESS 1

#define GET_ROOM_STATE_CODE 11
#define LEAVE_ROOM_CODE 12

//c'tor
RoomMemberRequestHandler::RoomMemberRequestHandler(RequestHandlerFactory* handlerFactory, LoggedUser user, Room& roomData) : RoomHandler(handlerFactory, user, roomData, false)
{

}

//return if the request is one of the relevants at this point
bool RoomMemberRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.RequestId == GET_ROOM_STATE_CODE || requestInfo.RequestId == LEAVE_ROOM_CODE;
}

//handle the request
RequestResult RoomMemberRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult reqResult;
	switch (requestInfo.RequestId)
	{
	case LEAVE_ROOM_CODE:
		reqResult = this->leaveRoom(requestInfo);
		break;
	case GET_ROOM_STATE_CODE:
		reqResult = this->getRoomState(requestInfo);
		break;
	}
	return reqResult;
}

/*
leave the room
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult RoomMemberRequestHandler::leaveRoom(RequestInfo requestInfo)
{
	RequestResult requestResult;
	LeaveRoomResponse leaveResponse;

	try
	{
		this->m_room.removeUser(this->m_user);
		if (this->m_room.getAllUsers().empty())
		{
			this->m_handlerFactory->getRoomManager().deleteRoom(this->m_room.getRoomData().id);
		}
		try
		{
			this->m_handlerFactory->getGameManager().getGame(this->m_user).removePlayer(this->m_user);
		}
		catch(std::exception e)
		{

		}
		leaveResponse.status = SUCCESS;
	}
	catch (std::exception e)
	{
		leaveResponse.status = FAILED;
		leaveResponse.err = e.what();
	}
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(leaveResponse);
	requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	return requestResult;
}

