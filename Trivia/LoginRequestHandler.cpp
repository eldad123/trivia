#include "LoginRequestHandler.h"
#include "RequestsStructs.h"
#include "JsonRequestPacketDeserializer.h"
#include "ResponseStructs.h"
#include "JsonResponsePacketSerializer.h"
#include "MenuRequestHandler.h"
#define LOGIN_ID 1
#define SIGN_UP_ID 0
#define FAILD 0
#define SUCCESS 1

//c'tor
LoginRequestHandler::LoginRequestHandler(IDatabase* dataAccess)
{
	this->m_handlerFactory = RequestHandlerFactory::instance(dataAccess);
}

//return if the request is one of the relevants at this point
bool LoginRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.RequestId == LOGIN_ID || requestInfo.RequestId == SIGN_UP_ID);
}

//handle the request
RequestResult LoginRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult reqResault;
	switch (requestInfo.RequestId)
	{
	case LOGIN_ID:
		reqResault = this->login(requestInfo);
		break;
	case SIGN_UP_ID:
		reqResault = this->signup(requestInfo);
		break;
	}
	return reqResault;
}

/*
log in
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult  LoginRequestHandler::login(RequestInfo requestInfo)
{
	RequestResult reqResault;
	LoginRequest loginRequest = JsonRequestPacketDeserializer::deserializeLoginRequest(requestInfo.buffer);
	try
	{
		this->m_handlerFactory->getLoginManager()->login(loginRequest.username, loginRequest.password);
		//if succeed
		reqResault.response = JsonResponsePacketSerializer::serializeResponse(LoginResponse{ SUCCESS });
		reqResault.newHandler = new MenuRequestHandler(loginRequest.username, this->m_handlerFactory);
	}
	catch (const std::exception & e)
	{
		//if faild
		reqResault.response = JsonResponsePacketSerializer::serializeResponse(LoginResponse{ FAILD, e.what() });
		reqResault.newHandler = nullptr;
	}
	return reqResault;
}

/*
signup
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult LoginRequestHandler::signup(RequestInfo requestInfo)
{
	RequestResult reqResault;
	SignupRequest signupRequest = JsonRequestPacketDeserializer::deserializeSignupRequest(requestInfo.buffer);
	try
	{
		this->m_handlerFactory->getLoginManager()->signup(signupRequest.username, signupRequest.password, 
			signupRequest.email, signupRequest.address, signupRequest.phoneNumber, signupRequest.birthday);
		//if success
		reqResault.response = JsonResponsePacketSerializer::serializeResponse(SignupResponse{ SUCCESS });
		reqResault.newHandler = new MenuRequestHandler(signupRequest.username,this->m_handlerFactory);
	}
	catch (const std::exception & e)
	{
		//if faild
		reqResault.response = JsonResponsePacketSerializer::serializeResponse(SignupResponse{ FAILD, e.what() });
		reqResault.newHandler = nullptr;
	}
	return reqResault;
}