#include "LoggedUser.h"

//c'tor
LoggedUser::LoggedUser(std::string username) 
{
	this->m_username = username;
}

//get the name of the user
std::string LoggedUser::getUsername() const
{
	return this->m_username;
}

//operators

bool LoggedUser::operator==(const LoggedUser& user) const
{
	return this->m_username == user.m_username;
}

bool LoggedUser::operator>(const LoggedUser& user) const
{
	return this->m_username > user.m_username;
}

bool LoggedUser::operator<(const LoggedUser& user) const
{
	return this->m_username < user.m_username;
}
