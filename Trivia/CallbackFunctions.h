#pragma once

//get data from db by callback function
int getUsernamOfUserFromDB(void* data, int argc, char** argv, char** azColName);
int callbackTotalTime(void* data, int argc, char** argv, char** azColName);
int callbackNumOfCorrectAnswers(void* data, int argc, char** argv, char** azColName);
int callbackNumOfTotalAnswers(void* data, int argc, char** argv, char** azColName);
int callbackNumOfPlayerGames(void* data, int argc, char** argv, char** azColName);
int callbackTop5(void* data, int argc, char** argv, char** azColName);
int callBackQuestions(void* data, int argc, char** argv, char** azColName);

