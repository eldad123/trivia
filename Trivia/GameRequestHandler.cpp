#include "GameRequestHandler.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#define FAILED 0
#define SUCCESS 1
#define GET_RESULTS_CODE 13
#define SUBMIT_ANSWER_CODE 14
#define GET_QUESTION_CODE 15
#define LEAVE_GAME_CODE 16

//c'tor
GameRequestHandler::GameRequestHandler(RequestHandlerFactory* handlerFactory, Game& game, LoggedUser user) : m_game(game), m_user(user.getUsername())
{
	this->m_handlerFactory = handlerFactory;
}

/*
Get index of correct answer in the vector of answers
input: question
output: index of correct answer in the vector of answers
*/
int GameRequestHandler::GetIndexOfCorrectAnswer(Question q)
{
	std::string correctAns = q.getCorrectAnswer();
	unsigned int correctAnsIndex = 0;
	for (int i = 0; i < q.getPossibleAnswersVec().size(); i++)
	{
		if (q.getPossibleAnswersVec()[i] == correctAns)
		{
			correctAnsIndex = i;
			break;
		}
	}
	return correctAnsIndex;
}

//return if the request is one of the relevants at this point
bool GameRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.RequestId == GET_RESULTS_CODE || requestInfo.RequestId == SUBMIT_ANSWER_CODE ||
		requestInfo.RequestId == GET_QUESTION_CODE || requestInfo.RequestId == LEAVE_GAME_CODE);
}

//handle the request
RequestResult GameRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult reqResult;
	switch (requestInfo.RequestId)
	{
	case GET_RESULTS_CODE:
		reqResult = this->getGameResults(requestInfo);
		break;
	case SUBMIT_ANSWER_CODE:
		reqResult = this->submitAnswer(requestInfo);
		break;
	case GET_QUESTION_CODE:
		reqResult = this->getQuestion(requestInfo);
		break;
	case LEAVE_GAME_CODE:
		reqResult = this->leaveGame(requestInfo);
		break;
	}
	return reqResult;
}

/*
get the question fir the user
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult GameRequestHandler::getQuestion(RequestInfo requestInfo)
{
	RequestResult reqResult;
	try
	{
		Question q = m_game.getQuestionForUser(this->m_user);
		//start calculate the averangeAnswerTime
		if (q == this->m_game.getQuestions()[0])
		{
			//auto it = this->m_game.getPlayers().find(this->m_user);
			//it->second.averangeAnswerTime = 1;			
		}
		std::vector<std::string> answersVec = q.getPossibleAnswersVec();
		std::map<int, std::string> answersMap;
		for (int i = 0; i < answersVec.size(); i++)
		{
			answersMap.insert(std::pair<int, std::string>(i, answersVec[i]));
		}
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse{ SUCCESS, q.getQuestion(), answersMap});
		reqResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(this->m_game, this->m_user);
	}
	catch (const std::exception & e)
	{
		//if faild
		std::map<int, std::string> answersMap;
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse{ FAILED, "", answersMap });
		reqResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(this->m_game, this->m_user);
	}
	return reqResult;
}

/*
submit user's answer
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult GameRequestHandler::submitAnswer(RequestInfo requestInfo)
{
	RequestResult requestResult;
	SubmitAnswerRequest submitAnswerReq = JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(requestInfo.buffer);
	Question q = m_game.getQuestionForUser(this->m_user);
	auto it = this->m_game.getPlayers().find(this->m_user);
	int correctAnsIndex = this->GetIndexOfCorrectAnswer(q);
	try
	{
		it->second.averangeAnswerTime += submitAnswerReq.answerTime;
		this->m_game.submitAnswer(this->m_user, submitAnswerReq.answerId);
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse{ SUCCESS, correctAnsIndex });
	}
	catch (std::exception & e)
	{
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse{ FAILED , correctAnsIndex });
	}

	if (q == this->m_game.getQuestions()[this->m_game.getQuestions().size() - 1])
	{
		it->second.averangeAnswerTime /= (it->second.correctAnswerCount + it->second.wrongAnswerCount);
	}
	requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(this->m_game, this->m_user);
	return requestResult;
}

/*
get game result
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult GameRequestHandler::getGameResults(RequestInfo requestInfo)
{
	RequestResult reqResult;
	try
	{
		std::vector<PlayerResults> playersResultVec;
		for (auto& player : this->m_game.getPlayers())
		{
			//if the player has finished, get his result
			std::vector<LoggedUser> connectUsers = this->m_game.GetConnectedPlayers();
			std::vector<LoggedUser>::iterator it = std::find(connectUsers.begin(),connectUsers.end(), player.first);
			
			if ((player.second.correctAnswerCount + player.second.wrongAnswerCount) == this->m_game.getQuestions().size() || it == connectUsers.end())
			{
				PlayerResults playerRusult{ player.first.getUsername() , player.second.correctAnswerCount,
							player.second.wrongAnswerCount, player.second.averangeAnswerTime };
				playersResultVec.push_back(playerRusult);
			}
			else
			{
				throw std::exception("Not all the players have finished");
			}
		}
		
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse{ SUCCESS, playersResultVec });
		reqResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(this->m_game, this->m_user);
	}
	catch (const std::exception & e)
	{
		std::vector<PlayerResults> playersResultVec;
		//if faild
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse{ FAILED, playersResultVec });
		reqResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(this->m_game, this->m_user);
	}
	return reqResult;
}

/*
leave a game
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult GameRequestHandler::leaveGame(RequestInfo requestInfo)
{
	RequestResult reqResult;
	try
	{
		Question q = m_game.getQuestionForUser(this->m_user);
		auto it = this->m_game.getPlayers().find(this->m_user);
		if (!(q == this->m_game.getQuestions()[this->m_game.getQuestions().size() - 1]))
		{
			it->second.averangeAnswerTime /= (it->second.correctAnswerCount + it->second.wrongAnswerCount);
		}

		this->m_handlerFactory->getStatisticsManager().updateStatistics(this->m_game.getPlayers().find(this->m_user)->second, this->m_game.getPlayers().find(this->m_user)->first);
		
		if (this->m_game.GetConnectedPlayers().size() == 1)
		{
			this->m_handlerFactory->getGameManager().deleteGame(&this->m_game);
			//delete the room
			std::vector<Room> allRooms = this->m_handlerFactory->getRoomManager().getRooms();
			for (auto& room : allRooms)
			{
				for (auto& name : room.getAllUsers())
				{
					if (name == this->m_user.getUsername())
					{
						this->m_handlerFactory->getRoomManager().deleteRoom(room.getRoomData().id);
					}
				}
			}
		}
		else
		{
			this->m_game.removePlayer(this->m_user);
			std::vector<Room> allRooms = this->m_handlerFactory->getRoomManager().getRooms();
			for (auto& room : allRooms)
			{
				for (auto& name : room.getAllUsers())
				{
					if (name == this->m_user.getUsername())
					{
						this->m_handlerFactory->getRoomManager().getRoomById(room.getRoomData().id).removeUser(this->m_user);
					}
				}
			}
		}

		reqResult.response = JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse{ SUCCESS });
		reqResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	catch (const std::exception & e)
	{
		//if faild
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse{ FAILED, e.what() });
		reqResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	return reqResult;
}

