#pragma once
#include "IRequestHandler.h"
#include "Room.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "RequestsStructs.h"
#include <iostream>
class RoomHandler
{
protected:
	Room& m_room; //room
	LoggedUser m_user; //user in room
	RequestHandlerFactory* m_handlerFactory;
	bool isAdmin;

public:
	RoomHandler(RequestHandlerFactory* handlerFactory, LoggedUser user, Room& roomData, bool isAdmin); //c'tor
	RequestResult getRoomState(RequestInfo requestInfo); //get the state of the room
};