#include "LoginManager.h"
#include <regex>
#define USERNAME_APOSTROPHE_ERROR "username: username cant has ' and \""
#define PASSWOR_LENGTH_ERROR "password: password has to be at least 8 characters length"
#define PASSWORD_COMPLEXITY_ERROR "password: password has to contain capital and lower letters, digit, one of those characters: [@\!\#\$\%\^\&\*] and must'nt contain ' and \""
#define EMAIL_ERROR "email: email has to be in the form: letters/digits@domain.domain and must'nt contain ' and \""
#define ADRESS_ERROR "address: address has to be in the form: [ (letters, digits, letters) ] and must'nt contain ' and \""
#define PHONE_NUMBER_LENGTH_ERROR "phoneNumber: phone numbers has to be 10 digits length for cellphone or 8 for city"
#define PHONE_NUMBER_FORMAT_ERROR "phoneNumber: phone numbers has to be in form: [0][1-9][0-9]"
#define BIRTHDAY_ERROR "birthday: birthday has to be in form: DD.MM.YYYY or DD/MM/YYYY"
#define USER_ALREADY_CONNECTED_ERROR "The user has already logged in"
#define SIGNIN_INCORRECT_DETAILS_ERROR "Incorrect details"
#define LOGOUT_ERROR "User has not logged in"
#define APOSTROPHE '\''
#define APOSTROPHES '\"'
#define DIDNT_FIND -1

/* Null, because instance will be initialized on demand. */
LoginManager* LoginManager::loginManager_instance = nullptr;

LoginManager* LoginManager::instance(IDatabase* dataAccess)
{
	if (!loginManager_instance)
		loginManager_instance = new LoginManager(dataAccess);
	return loginManager_instance;
}

LoginManager::LoginManager(IDatabase* dataAccess) :
	m_database(dataAccess)
{
	// Left empty
	m_database->open();
}

bool LoginManager::isUsernameValid(std::string username)
{
	if (username.find(APOSTROPHE) != DIDNT_FIND
		|| username.find(APOSTROPHES) != DIDNT_FIND)
	{
		throw std::exception(USERNAME_APOSTROPHE_ERROR);
	}
	return true;
}

//return if the password is valid
bool LoginManager::isPasswordValid(std::string password)
{
	//check length
	std::regex cellPhoneLen(".{8,}");
	if (!(regex_match(password, cellPhoneLen)))
	{
		throw std::exception(PASSWOR_LENGTH_ERROR);
	}

	//check if the password has capital, lower letters & digit
	std::regex upperAndLower("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).+$");
	std::cmatch result1;

	//check if the password has the special letters
	std::regex specialChars{ R"([@\!\#\$\%\^\&\*])" };
	std::cmatch result2;

	if (!std::regex_search(password.c_str(), result1, upperAndLower) || 
		!std::regex_search(password.c_str(), result2, specialChars) || password.find(APOSTROPHE) != DIDNT_FIND
		|| password.find(APOSTROPHES) != DIDNT_FIND)
	{
		throw std::exception(PASSWORD_COMPLEXITY_ERROR);
	}


	return true;
}

//check if the email is on something@ending.ending
bool LoginManager::isEmailValid(std::string email)
{
	const std::regex pattern("(\\w+)?(\\w*)@(\\w+)(\\.(\\w+))+");
	if (!regex_match(email, pattern) || email.find(APOSTROPHE) != DIDNT_FIND
		|| email.find(APOSTROPHES) != DIDNT_FIND)
	{
		throw std::exception(EMAIL_ERROR);
	}
	return true;
}

//check if the addrrss is on format (Street, Apt, City)
bool LoginManager::isAddressValid(std::string address)
{
	const std::regex pattern("[(][a-z|A-Z]+[,]+[ ]+[[:digit:]]+[,]+[ ]+[a-z|A-Z]+[)]");
	if (!regex_match(address, pattern) || address.find(APOSTROPHE) != DIDNT_FIND
		|| address.find(APOSTROPHES) != DIDNT_FIND)
	{
		throw std::exception(ADRESS_ERROR);
	}
	return true;
}

//return if the phone number is valid
bool LoginManager::isPhoneNumberValid(std::string phoneNumber)
{
	std::regex cityNumberlen(".{9,}");
	std::regex cellPhoneLen(".{10,}");
	std::regex pattern("[0][1-9][0-9]+");

	if (!(regex_match(phoneNumber, cityNumberlen) || regex_match(phoneNumber, cellPhoneLen)))
	{
		throw std::exception(PHONE_NUMBER_LENGTH_ERROR);
	}
	if (!regex_match(phoneNumber, pattern))
	{
		throw std::exception(PHONE_NUMBER_FORMAT_ERROR);
	}

	return true;
}

//check if the date is on format: DD.MM.YYYY or DD/MM/YYYY
bool LoginManager::isBirthdayValid(std::string birthday)
{
	std::regex pattern("^(?:(?:31(\\/|-|\\.)(?:0?[13578]|1[02]))\\1|(?:(?:29|30)(\\/|-|\\.)(?:0?[13-9]|1[0-2])\\2))(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$|^(?:29(\\/|-|\.)0?2\\3(?:(?:(?:1[6-9]|[2-9]\\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\\d|2[0-8])(\\/|-|\\.)(?:(?:0?[1-9])|(?:1[0-2]))\\4(?:(?:1[6-9]|[2-9]\\d)?\\d{2})$");
	if (!regex_match(birthday, pattern))
	{
		throw std::exception(BIRTHDAY_ERROR);
	}
	return true;
}

//sign up by all the needed parameters
void LoginManager::signup(std::string username, std::string password, std::string email, 
	std::string address, std::string phoneNumber, std::string birthday)
{
	if ((this->isUsernameValid(username)) && (this->isPasswordValid(password)) && (this->isEmailValid(email) && (this->isAddressValid(address)) &&
		this->isPhoneNumberValid(phoneNumber) && this->isBirthdayValid(birthday)))
	{
		m_database->addNewUser(username, password, email, address, phoneNumber, birthday);
	}

}
 
//sign in by all the needed parameters
void LoginManager::login(std::string username, std::string password)
{
	std::unique_lock<std::mutex> connect_users_lock(this->_mtx, std::defer_lock);
	if (m_database->doesUserExist(username) && m_database->doesPasswordMatch(username, password))
	{
		LoggedUser user(username);
		auto it = std::find(m_loggedUsers.begin(), m_loggedUsers.end(), user);

		if (it != m_loggedUsers.end())
		{
			throw std::exception(USER_ALREADY_CONNECTED_ERROR);
		}
		else
		{
			connect_users_lock.lock();
			m_loggedUsers.push_back(user);
			connect_users_lock.unlock();
		}
	}
	else
	{
		throw std::exception(SIGNIN_INCORRECT_DETAILS_ERROR);
	}

}

//logout by all the needed parameters
void LoginManager::logout(std::string username)
{
	std::unique_lock<std::mutex> connect_users_lock(this->_mtx, std::defer_lock);
	if (m_database->doesUserExist(username))
	{
		LoggedUser user(username);
		auto it = std::find(m_loggedUsers.begin(), m_loggedUsers.end(), user);

		if (it != m_loggedUsers.end())
		{
			for (int i = 0; i < m_loggedUsers.size(); i++)
			{
				if (username == m_loggedUsers[i].getUsername())
				{
					connect_users_lock.lock();
					m_loggedUsers.erase(m_loggedUsers.begin() + i);
					connect_users_lock.unlock();
				}
			}
		}
		else
		{
			throw std::exception(LOGOUT_ERROR);
		}
	}
}

