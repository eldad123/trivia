#pragma once
#include <iostream>
#include "json.hpp"
#include "IDatabase.h"
//the winner is the one who answer more questions correctly and if there is a tie so the average time for question will decide who win
class StatisticsManager
{
private:
	IDatabase* m_database; //database
public:
	StatisticsManager(IDatabase* database); //c'tor

	//get and update statistics
	std::vector<std::vector<std::string>> getStatistics(std::string username);
	std::vector<std::vector<std::string>> getHighScore();
	std::vector<std::string> getUserStatistics(std::string username);
	void updateStatistics(GameData gamedata, LoggedUser user);
};