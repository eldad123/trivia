#pragma once
#include <vector>
#include "RequestsStructs.h"

class JsonRequestPacketDeserializer
{
public:
	//deserealization of some requests
	static LoginRequest deserializeLoginRequest(std::vector<unsigned char> buffer);
	static SignupRequest deserializeSignupRequest(std::vector<unsigned char> buffer);

	static GetPlayersInRoomRequest deserializeGetPlayersRequest(std::vector<unsigned char> buffer);
	static JoinRoomRequest deserializeJoinRoomRequest(std::vector<unsigned char> buffer);
	static CreateRoomRequest deserializeCreateRoomRequest(std::vector<unsigned char> buffer);

	static SubmitAnswerRequest deserializeSubmitAnswerRequest(std::vector<unsigned char> buffer);

};