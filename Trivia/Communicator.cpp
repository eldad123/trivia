#include "Communicator.h"
#include "LoginRequestHandler.h"
#include <iomanip>
#include <sstream>
#include <WinSock2.h>
#include <iostream>
#include "JsonRequestPacketDeserializer.h"
#include "ResponseStructs.h"
#include "JsonResponsePacketSerializer.h"
#define CODE_BYTES_SIZE 1
#define DATA_BYTES_SIZE 4

/* Null, because instance will be initialized on demand. */
Communicator* Communicator::communicator_instance = nullptr;

Communicator* Communicator::instance(IDatabase* dataAccess)
{
	if (!communicator_instance)
		communicator_instance = new Communicator(dataAccess);
	return  communicator_instance;
}

//c'tor
Communicator::Communicator(IDatabase* dataAccess) 
{
	this->m_handlerFacroty = RequestHandlerFactory::instance(dataAccess);
	// this server use TCP. that why SOCK_STREAM & IPPROTO_TCP
	// if the server use UDP we will use: SOCK_DGRAM & IPPROTO_UDP
	this->_serverSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (this->_serverSocket == INVALID_SOCKET)
		throw std::exception(__FUNCTION__ " - socket");

}

//d'tor
Communicator::~Communicator()
{
	try
	{
		// the only use of the destructor should be for freeing 
		// resources that was allocated in the constructor
		closesocket(this->_serverSocket);
	}
	catch (...) {}
}

/*
function will send data to the client
input: client socket and message
output: none
*/
void Communicator::sendData(SOCKET sc, std::string message)
{
	const char* data = message.c_str();
	if (send(sc, data, message.size(), 0) == INVALID_SOCKET)
	{
		throw std::exception("Error while sending message to client");
	}
}

//get client connection request
void Communicator::StartHandleRequests()
{
	SOCKET client_socket;
	std::unique_lock<std::mutex> connect_users_lock(this->_mtx_for_connect_users, std::defer_lock);
	this->bindAndListen();
	while (true)
	{
		// the main thread is only accepting clients 
		// and add then to the list of handlers
		std::cout << "Waiting for client connection request" << std::endl;
		// this accepts the client and create a specific socket from server to this client
		client_socket = ::accept(_serverSocket, NULL, NULL);

		if (client_socket == INVALID_SOCKET)
			throw std::exception(__FUNCTION__);
		std::thread t(&Communicator::handleNewClient, this, client_socket);
		t.detach();
		connect_users_lock.lock();
		this->m_clients.insert(std::pair<SOCKET, IRequestHandler*>(client_socket, this->m_handlerFacroty->createLoginRequestHandler()));
		connect_users_lock.unlock();
	}
}

/*
function will recieve data from the client
input: client socket and bytes number of the message
output: the message
*/
char* Communicator::receiveData(SOCKET client_socket, int bytesNum)
{
	if (bytesNum == 0)
	{
		return (char*)"";
	}
	char* data = new char[bytesNum + 1];
	int res = recv(client_socket, data, bytesNum, 0);
	data[bytesNum] = '\0';
	if (res == INVALID_SOCKET)
	{
		std::string s = "Error while recieving from socket: ";
		s += std::to_string(client_socket);
		throw std::exception(s.c_str());
	}
	return data;
}

/*
This function turn on the server in order to handle clients
input: none
output:none
*/

void Communicator::bindAndListen()
{

	struct sockaddr_in sa = { 0 };

	sa.sin_port = htons(PORT); // port that server will listen for
	sa.sin_family = AF_INET;   // must be AF_INET
	sa.sin_addr.s_addr = INADDR_ANY;    // when there are few ip's for the machine. We will use always "INADDR_ANY"

	// again stepping out to the global namespace
	// Connects between the socket and the configuration (port and etc..)
	if (bind(_serverSocket, (struct sockaddr*) & sa, sizeof(sa)) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - bind");

	// Start listening for incoming requests of clients
	if (listen(_serverSocket, SOMAXCONN) == SOCKET_ERROR)
		throw std::exception(__FUNCTION__ " - listen");
	std::cout << "Listening on port " << PORT << std::endl;

}

//This function create a thread that handle the client
//TO TAKE OT OUT TO LoginRequestHandler
void Communicator::handleNewClient(SOCKET client_socket)
{
	std::unique_lock<std::mutex> connect_users_lock(this->_mtx_for_connect_users, std::defer_lock);
	RequestInfo reqInfo;
	RequestResult reqResault;
	IRequestHandler* requestHandler;
	std::vector<unsigned char> messageToSend;
	int relevant = false;
	std::string responseMessage = "Hello";
	this->sendData(client_socket, responseMessage);
	try
	{
		while (true)
		{
			connect_users_lock.lock();
			requestHandler = this->m_clients.find(client_socket)->second;
			connect_users_lock.unlock();
			reqInfo = this->getRequestInfoStructFromSocket(client_socket);
			relevant = requestHandler->isRequestRelevant(reqInfo);
			//if the message is relevant to this part
			if (relevant)
			{
				connect_users_lock.lock();
				reqResault = requestHandler->handleRequest(reqInfo);
				connect_users_lock.unlock();
				//if the request faild
				if (reqResault.newHandler != nullptr)
				{
					delete requestHandler;
					connect_users_lock.lock();
					this->m_clients.find(client_socket)->second = reqResault.newHandler;
					connect_users_lock.unlock();
				}
				responseMessage = std::string(reqResault.response.begin(), reqResault.response.end());
			}
			else
			{
				messageToSend = JsonResponsePacketSerializer::serializeResponse(ErrorResponse{ "The message in not valid at this point" });
				responseMessage = std::string(messageToSend.begin(), messageToSend.end());
			}
			this->sendData(client_socket, responseMessage);
		}
	}
	catch (const std::exception& e)
	{
		//delete the user from the list of connected users
		connect_users_lock.lock();
		delete this->m_clients.find(client_socket)->second;
		this->m_clients.erase(client_socket);
		connect_users_lock.unlock();
		std::cout << "a client was disconnected" << std::endl;
		// Closing the socket (in the level of the TCP protocol)
		closesocket(client_socket);
	}

}

/*
function will turn the request to RequestInfo
input: client socket
output: RequestInfo
*/
RequestInfo Communicator::getRequestInfoStructFromSocket(SOCKET client_socket)
{
	//RequestInfo* reqInfo = new RequestInfo;
	RequestInfo reqInfo;
	int dataLen = 0;
	//set the code of the message
	char* code = this->receiveData(client_socket, CODE_BYTES_SIZE);
	memcpy(&reqInfo.RequestId, code, sizeof(reqInfo.RequestId));
	//set the current time
	reqInfo.receivalTime = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
	//set the data from the client message
	char* dataLength = this->receiveData(client_socket, DATA_BYTES_SIZE);
	memcpy(&dataLen, dataLength, sizeof(char) * 4);
	std::string data = std::string(this->receiveData(client_socket, dataLen));
	reqInfo.buffer = std::vector<unsigned char>(data.begin(), data.end());
	delete code;
	delete dataLength;
	return reqInfo;
}