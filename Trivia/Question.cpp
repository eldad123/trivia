#include "Question.h"
#define INDEX_OF_CORRECT_ANSWER 0

//c'tor
Question::Question(std::string question, std::string correctAnswer, std::string wrongAnswer1, std::string wrongAnswer2, std::string wrongAnswer3)
{
	this->m_question = question;
	this->correctAnswer = correctAnswer;
	this->m_possibleAnswers.push_back(correctAnswer);
	this->m_possibleAnswers.push_back(wrongAnswer1);
	this->m_possibleAnswers.push_back(wrongAnswer2);
	this->m_possibleAnswers.push_back(wrongAnswer3);
}

//get the question
std::string Question::getQuestion()
{
	return this->m_question;
}

//get the possible answers in string
std::string Question::getPossibleAnswers()
{
	std::string possibleAnswers = "";
	for (auto& answer : this->m_possibleAnswers)
	{
		possibleAnswers += answer + ",";
	}
	possibleAnswers.pop_back();
	return possibleAnswers;
}

//get the possible answers in vector
std::vector<std::string>& Question::getPossibleAnswersVec()
{
	return this->m_possibleAnswers;
}

//get the correct answer
std::string Question::getCorrectAnswer()
{
	return this->correctAnswer;
}


bool Question::operator==(const Question& other) const
{
	if (this->m_question == other.m_question)
	{
		for (int i = 0; i < this->m_possibleAnswers.size(); i++)
		{
			if (this->m_possibleAnswers[i] != other.m_possibleAnswers[i])
			{
				return false;
			}
		}
		return true; //if all the answers were the same
	}
	else
	{
		return false;
	}
}