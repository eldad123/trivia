#include "Server.h"
#include <exception>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>

/* Null, because instance will be initialized on demand. */
Server* Server::server_instance = nullptr;

Server* Server::instance(IDatabase* dataAccess)
{
	if (!server_instance)
		server_instance = new Server(dataAccess);
	return server_instance;
}

//c'tor
Server::Server(IDatabase* dataAccess)
{
	this->m_database = dataAccess;
	this->m_communicator = Communicator::instance(dataAccess);
	this->m_handlerFactory = RequestHandlerFactory::instance(dataAccess);
}

//d'tor
Server::~Server()
{
	
}

//funtion will create thred of handling client
void Server::run()
{
	std::string message = "";
	std::thread t(&Communicator::StartHandleRequests, this->m_communicator);
	t.detach();
	while (message != "EXIT")
	{
		std::cin >> message;
	}
}

