#pragma once
#include <iostream>
#include <vector>
#include <map>
#include "LoggedUser.h"
#include "Room.h"

class RoomManager
{

public:
	void createRoom(LoggedUser userCreatedRoom, RoomData roomData); //create new room
	void deleteRoom(int ID); //delete room by his id
	Room& getRoomById(int ID); //get room by his id
	unsigned int getRoomState(int ID); //get room state by his id
	std::vector<Room> getRooms(); //get all the rooms

private:
	std::map<int, Room*> m_rooms;//map of rooms id and rooms
	int _lastRoomId = 0;//the last id of the last room that created
};