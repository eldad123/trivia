#pragma once
#include <iostream>

class LoggedUser
{
public:
	LoggedUser(std::string username); //c'tor
	std::string getUsername() const; //get the username of the user
	bool operator==(const LoggedUser& user) const;
	bool operator>(const LoggedUser& user) const;
	bool operator<(const LoggedUser& user) const;

private:
	std::string m_username;
};