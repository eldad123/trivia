#pragma once
#include <WinSock2.h>
#include <Windows.h>
#include <mutex>
#include <map>
#include "IRequestHandler.h"
#include <iostream>
#include "RequestsStructs.h"
#include "RequestHandlerFactory.h"
#define PORT 3000
class Communicator
{

public:
	~Communicator(); //d'tor
	void bindAndListen(); //function turn on the server in order to handle clients
	void StartHandleRequests(); //client connection request
	void handleNewClient(SOCKET client_socket); //handle clients
	void sendData(SOCKET sc, std::string message); //send data to the client
	char* receiveData(SOCKET client_socket, int bytesNum); //receive data from the client
	/*
	Static function in order to create the first instance if needed and get it
	input: the IDataBase class that use the db
	output: the instance of the object
	*/
	static Communicator* instance(IDatabase* dataAccess);

private:
	/* Private constructor to prevent instancing. */
	Communicator(IDatabase* dataAccess);
	/* Here will be the instance stored. */
	static Communicator* communicator_instance;
	std::mutex _mtx_for_connect_users;
	std::map<SOCKET, IRequestHandler*> m_clients; //socket's clients and thier handler
	RequestHandlerFactory* m_handlerFacroty;
	SOCKET _serverSocket;
	RequestInfo getRequestInfoStructFromSocket(SOCKET client_socket); //turn the request to RequestInfo


};