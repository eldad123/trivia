#pragma comment(lib,"Ws2_32.lib")
#include "Server.h"
#include <iostream>
#include <exception>
#include "WSAInitializer.h"
#include "SqliteDataBase.h"
//to delete after test
#include "JsonResponsePacketSerializer.h"
#include "RequestsStructs.h"
int main()
{
	try
	{
		WSAInitializer wsaInit;
		SqliteDataBase* sqlDataBade = SqliteDataBase::instance();
		Server*  myServer = Server::instance(sqlDataBade);
		//make a thread that write messages to the files
		myServer->run();


	}
	catch (std::exception & e)
	{
		std::cout << "Error occured: " << e.what() << std::endl;
	}
	system("PAUSE");
	return 0;
}
