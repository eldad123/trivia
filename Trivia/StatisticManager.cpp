#include "StatisticManager.h"
#include "LoggedUser.h"
#include "ResponseStructs.h"
#define NUMBER_OF_BEST_SCORES 5

//c'tor
StatisticsManager::StatisticsManager(IDatabase* database)
{
	this->m_database = database;
}

//get all the atatistics of input username
std::vector<std::vector<std::string>> StatisticsManager::getStatistics(std::string username)
{
	std::vector<std::vector<std::string>> fullStatisticResponse;
	fullStatisticResponse.push_back(this->getUserStatistics(username));
	std::vector<std::vector<std::string>> highScores = this->getHighScore();
	std::copy(highScores.begin(), highScores.end(), std::back_inserter(fullStatisticResponse));
	return fullStatisticResponse;
}

//get the high score
std::vector<std::vector<std::string>> StatisticsManager::getHighScore()
{
	std::vector<std::vector<std::string>> heighScore;
	std::vector<TopUser> top5 = this->m_database->getTopFiveScores();
	for (auto& topUser : top5)
	{
		heighScore.push_back(std::vector<std::string>{topUser.username, std::to_string(topUser.score)});
	}
	return heighScore;
}

//get all the input user statistics from the db
std::vector<std::string> StatisticsManager::getUserStatistics(std::string username)
{
	std::vector<std::string> statistics;
	//correct_answers
	statistics.push_back(std::to_string(this->m_database->getNumOfCorrectAnswers(username)));
	//player_games
	statistics.push_back(std::to_string(this->m_database->getNumOfPlayerGames(username)));
	//total_answers
	statistics.push_back(std::to_string(this->m_database->getNumOfTotalAnswers(username)));
	//average_time_for_answer
	statistics.push_back(std::to_string(this->m_database->getPlayerAverageAnswerTime(username)));
	return statistics;
}

/*
update the user statistics
input: game data that will be the latest statistics and the user himself
output: none
*/
void StatisticsManager::updateStatistics(GameData gamedata, LoggedUser user)
{
	this->m_database->updateStatistics(gamedata, user);
}