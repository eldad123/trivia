#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"
#include <mutex>

class GameRequestHandler : public IRequestHandler
{
private:
	int GetIndexOfCorrectAnswer(Question q); //Get index of correct answer
	RequestHandlerFactory* m_handlerFactory;
	Game& m_game; //the game
	LoggedUser m_user;

public:
	GameRequestHandler(RequestHandlerFactory* handlerFactory, Game& game, LoggedUser user); //c'tor
	//d'tor
	virtual ~GameRequestHandler()
	{

	}
	virtual bool isRequestRelevant(RequestInfo requestInfo); //check if the request is valid in the point of the conecting
	virtual RequestResult handleRequest(RequestInfo requestInfo); //handle the request
	//actions of game
	RequestResult getQuestion(RequestInfo requestInfo);
	RequestResult submitAnswer(RequestInfo requestInfo);
	RequestResult getGameResults(RequestInfo requestInfo);
	RequestResult leaveGame(RequestInfo requestInfo);

};