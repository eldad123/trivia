#include "MenuRequestHandler.h"
#include "ResponseStructs.h"
#include "JsonResponsePacketSerializer.h"
#include "JsonRequestPacketDeserializer.h"
#include <iostream>
#define FAILED 0
#define SUCCESS 1
#define LOGOUT_CODE 3
#define CREATE_ROOM_CODE 4
#define JOIN_ROOM_CODE 5
#define GET_ROOMS_CODE 6
#define GET_PLAYERS_CODE 7
#define GET_STATS_CODE 8
#define IS_NOT_ACTIVE 0 

#define NO_ID_ERROR "no id like this"
#define ACTIVE_ERROR "A game has started"
#define ACTIVE_ERROR_CODE -1
#define NO_PLACE_ERROR "No more place in the room"
#define NO_PLACE_ERROR_CODE  -2

//c'tor
MenuRequestHandler::MenuRequestHandler(std::string username, RequestHandlerFactory* factory) : m_user(username)
{
	this->m_handlerFactory = factory;
}

//return if the request is one of the relevants at this point
bool MenuRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return (requestInfo.RequestId == LOGOUT_CODE || requestInfo.RequestId == CREATE_ROOM_CODE ||
		requestInfo.RequestId == JOIN_ROOM_CODE || requestInfo.RequestId == GET_ROOMS_CODE ||
		requestInfo.RequestId == GET_PLAYERS_CODE || requestInfo.RequestId == GET_STATS_CODE);
}

//handle the request
RequestResult MenuRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult reqResult;

	switch (requestInfo.RequestId)
	{
	case LOGOUT_CODE:
		reqResult = this->signout(requestInfo);
		break;
	case CREATE_ROOM_CODE:
		reqResult = this->createRoom(requestInfo);
		break;
	case JOIN_ROOM_CODE:
		reqResult = this->joinRoom(requestInfo);
		break;
	case GET_ROOMS_CODE:
		reqResult = this->getRooms(requestInfo);
		break;
	case GET_PLAYERS_CODE:
		reqResult = this->getPlayersInRoom(requestInfo);
		break;
	case GET_STATS_CODE:
		reqResult = this->getStatistics(requestInfo);
		break;

	}
	return reqResult;
}

/*
get the players in specific room
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult MenuRequestHandler::getPlayersInRoom(RequestInfo requestInfo)
{
	RequestResult requestResult;
	GetPlayersInRoomRequest getPlayersReq = JsonRequestPacketDeserializer::deserializeGetPlayersRequest(requestInfo.buffer);
	std::vector<std::string> loggedUserInRoom;
	try
	{
		std::vector<Room> allRooms = this->m_handlerFactory->getRoomManager().getRooms();
		for (int i = 0; i < allRooms.size(); i++)
		{
			if (allRooms[i].getRoomData().id == getPlayersReq.roomId)
			{
				loggedUserInRoom = allRooms[i].getAllUsers();
				break;
			}
		}
	}
	catch (std::exception & e)
	{

	}
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse{ loggedUserInRoom });
	requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	return requestResult;
}

/*
get all the rooms
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult MenuRequestHandler::getRooms(RequestInfo requestInfo)
{
	RequestResult reqResult;
	try
	{
		std::vector<Room> allRooms = this->m_handlerFactory->getRoomManager().getRooms();
		std::vector<RoomData> allRoomData;
		for (int i = 0; i < allRooms.size(); i++)
		{
			allRoomData.push_back(allRooms[i].getRoomData());
		}

		//if succeed
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse{ SUCCESS, allRoomData });
		reqResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	catch (const std::string & e)
	{
		std::vector<RoomData> allRoomData;

		//if faild
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse{ FAILED, allRoomData });
		reqResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	return reqResult;
}

/*
join room
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult MenuRequestHandler::joinRoom(RequestInfo requestInfo)
{
	RequestResult requestResult;
	JoinRoomRequest joinRoomReq = JsonRequestPacketDeserializer::deserializeJoinRoomRequest(requestInfo.buffer);
	try
	{
		bool added = false;
		std::vector<Room> allRooms = this->m_handlerFactory->getRoomManager().getRooms();
		for (int i = 0; i < allRooms.size(); i++)
		{
			if (allRooms[i].getRoomData().id == joinRoomReq.roomId)
			{
				if (!(allRooms[i].getRoomData().isActive == IS_NOT_ACTIVE))
				{
					throw std::exception(ACTIVE_ERROR);
				}
				this->m_handlerFactory->getRoomManager().getRoomById(joinRoomReq.roomId).addUser(LoggedUser(this->m_user.getUsername()));
				requestResult.response = JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse{ SUCCESS });
				requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createRoomMemberRequestHandler(this->m_user, this->m_handlerFactory->getRoomManager().getRoomById(joinRoomReq.roomId));
				added = true;
				break;
			}
		}
		if (!added)
		{
			throw std::exception(NO_ID_ERROR);
		}
	}
	catch (std::exception & e)
	{
		int status = FAILED;
		if (std::string(e.what()) == ACTIVE_ERROR)
		{
			status = ACTIVE_ERROR_CODE;
		}
		else if (std::string(e.what()) == NO_PLACE_ERROR)
		{
			status = NO_PLACE_ERROR_CODE;
		}
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse{ status});
		requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	return requestResult;
}

/*
create room
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult MenuRequestHandler::createRoom(RequestInfo requestInfo)
{
	RequestResult requestResult;
	CreateRoomRequest createRoomReq = JsonRequestPacketDeserializer::deserializeCreateRoomRequest(requestInfo.buffer);
	try
	{
		RoomData roomData{
			0,
			createRoomReq.roomName,
			createRoomReq.maxUsers,
			createRoomReq.answerTimeout,
			createRoomReq.questionCount,
			IS_NOT_ACTIVE
		};

		this->m_handlerFactory->getRoomManager().createRoom(LoggedUser(this->m_user.getUsername()), roomData);
		int roomID = 0;
		std::vector<Room> allRooms = this->m_handlerFactory->getRoomManager().getRooms();
		for (int i = 0; i < allRooms.size(); i++)
		{
			//if this user is the admin - the new handler is of admin
			if (allRooms[i].getAllUsers()[0] == this->m_user.getUsername())
			{
				roomID = allRooms[i].getRoomData().id;
				requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createRoomAdminRequestHandler(this->m_user, this->m_handlerFactory->getRoomManager().getRoomById(roomID));
			}
		}
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse{ SUCCESS });
	}
	catch (std::exception & e)
	{
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse{ FAILED});
		requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	return requestResult;
}

/*
sign out
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult MenuRequestHandler::signout(RequestInfo requestInfo)
{
	RequestResult reqResult;
	try
	{
		this->m_handlerFactory->getLoginManager()->logout(this->m_user.getUsername());
		//if succeed
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(LogoutResponse{ SUCCESS });
		reqResult.newHandler = (IRequestHandler*)m_handlerFactory->createLoginRequestHandler();
	}
	catch (const std::exception & e)
	{
		//if faild
		reqResult.response = JsonResponsePacketSerializer::serializeResponse(LogoutResponse{ FAILED, e.what() });
		reqResult.newHandler = m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	return reqResult;
}

/*
get statistics
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult MenuRequestHandler::getStatistics(RequestInfo requestInfo)
{
	RequestResult requestResult;
	GetStatisticsResponse statisticsResponse;
	try
	{
		statisticsResponse.statistics = this->m_handlerFactory->getStatisticsManager().getStatistics(this->m_user.getUsername());
		statisticsResponse.status = SUCCESS;
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(statisticsResponse);
		requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	catch (std::exception & e)
	{
		statisticsResponse.status = FAILED;
		requestResult.response = JsonResponsePacketSerializer::serializeResponse(statisticsResponse);
		requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	return requestResult;
}