#include "RoomAdminRequestHandler.h"
#include "JsonRequestPacketDeserializer.h"
#include "JsonResponsePacketSerializer.h"
#include "RequestsStructs.h"
#include "ResponseStructs.h"

#define FAILED 0
#define SUCCESS 1
#define CLOSE_ROOM_CODE 9
#define START_GAME_CODE 10
#define GET_ROOM_STATE_CODE 11
#define IS_NOT_ACTIVE 0 
#define IS_ACTIVE 1

//c'tor
RoomAdminRequestHandler::RoomAdminRequestHandler(RequestHandlerFactory* handlerFactory, LoggedUser user, Room& roomData) : RoomHandler(handlerFactory, user, roomData, true)
{

}

//return if the request is one of the relevants at this point
bool RoomAdminRequestHandler::isRequestRelevant(RequestInfo requestInfo)
{
	return requestInfo.RequestId == CLOSE_ROOM_CODE || requestInfo.RequestId == START_GAME_CODE || requestInfo.RequestId == GET_ROOM_STATE_CODE;
}

//handle the request
RequestResult RoomAdminRequestHandler::handleRequest(RequestInfo requestInfo)
{
	RequestResult reqResult;
	switch (requestInfo.RequestId)
	{
	case CLOSE_ROOM_CODE:
		reqResult = this->closeRoom(requestInfo);
		break;
	case START_GAME_CODE:
		reqResult = this->startGame(requestInfo);
		break;
	case GET_ROOM_STATE_CODE:
		reqResult = this->getRoomState(requestInfo);
		break;
	}
	return reqResult;
}

/*
cliose room
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult RoomAdminRequestHandler::closeRoom(RequestInfo requestInfo)
{
	RequestResult requestResult;
	LeaveRoomResponse leaveResponse{ SUCCESS };
	try
	{
		this->m_room.removeUser(this->m_user);
		if (this->m_room.getAllUsers().empty())
		{
			this->m_handlerFactory->getRoomManager().deleteRoom(this->m_room.getRoomData().id);
		}
	}
	catch (std::exception& e)
	{
		leaveResponse.status = FAILED;
		leaveResponse.err = e.what();
	}
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(leaveResponse);
	requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	return requestResult;
}

/*
start game
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult RoomAdminRequestHandler::startGame(RequestInfo requestInfo)
{
	RequestResult requestResult;
	StartGameResponse startResponse{ SUCCESS };
	try
	{
		this->m_room.getRoomData().isActive = IS_ACTIVE ;
	}
	catch (std::exception & e)
	{
		startResponse.status = FAILED;
	}
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(startResponse);
	requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(
		this->m_handlerFactory->getGameManager().createGame(this->m_room), this->m_user);
	return requestResult;
}


