#pragma once
#include <iostream>
#include <vector>
#include "Question.h"
#include <map>
#include "LoggedUser.h"
#include "DataStructs.h"


class Game
{
public:
	Game(std::vector<Question> questions, std::map<LoggedUser, GameData> players, std::vector<LoggedUser> connect_users); //c'tor
	Question getQuestionForUser(LoggedUser user); //get the current question of user
	void submitAnswer(LoggedUser user, int userAnsIndex); //submit the answer 
	void removePlayer(LoggedUser user); //remove the player from the game
	bool operator==(const Game& other) const;
	std::map<LoggedUser, GameData>& getPlayers();
	std::vector<Question> getQuestions() const;
	int GetCurrentQuestionIndex(Question q); //Get current question index in the question vector
	std::vector<LoggedUser> GetConnectedPlayers() const;


private:
	std::vector<Question> m_questions; //questions
	std::map<LoggedUser, GameData> m_players; //user and their game data
	std::vector<LoggedUser> connected_uers;
};