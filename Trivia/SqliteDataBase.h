#pragma once
#include "IDatabase.h"
#include "CallbackFunctions.h"
#include <iostream>
#include <vector>
#include "json.hpp"
#include "ResponseStructs.h"
class SqliteDataBase : public IDatabase
{
public:
	virtual bool open(); //open the db

    //methods of get and update db
	virtual bool doesUserExist(std::string username);
	virtual bool doesPasswordMatch(std::string username, std::string password);

	virtual void addNewUser(std::string username, std::string password, std::string email,
		std::string address, std::string phoneNumber, std::string birthday);
	void checkQuerySqliteExec(std::string query, std::string exception);
	virtual std::list<Question> getQuestions(int numberOfQuestions);
	virtual void updateStatistics(GameData gameData, LoggedUser user);
	virtual void close();
	virtual double getPlayerAverageAnswerTime(std::string username);
	virtual int getNumOfCorrectAnswers(std::string username);
	virtual int getNumOfTotalAnswers(std::string username);
	virtual int getNumOfPlayerGames(std::string username);
	virtual std::vector<TopUser> getTopFiveScores();
	/*
	Static function in order to create the first instance if needed and get it
	input: none
	output: the instance of the object
	*/
	static SqliteDataBase* instance();

private:
	/* Private constructor to prevent instancing. */
	SqliteDataBase() = default;
	//create an insert sql command for a question
	std::string getInsertCommand(nlohmann::json data, int questionIndex);
	//get the full json from the API(from the server)
	std::string getJsonOfQuestions();
	//add all the questions to the db
	bool addQuestionsToDB(std::string jsonString);
	//create the table and add the questions
	bool createQuestionTableAndAddQuestions();
	//replace the codes that appear in the answer that recieved from the API
	std::string replaceCodesFromTheAPI(std::string string);
	/* Here will be the instance stored. */
	static SqliteDataBase* sql_instance;
	sqlite3* _db;
	char** _error = nullptr;
	void executionSqlCommand(std::string sqlStatement, int (*callback)(void*, int, char**, char**), void* param, std::string exeptionInfo);
};