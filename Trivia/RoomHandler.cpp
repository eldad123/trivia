#include "RoomHandler.h"
#include "JsonResponsePacketSerializer.h"

#define FAILED 0
#define SUCCESS 1

#define ADMIN_LEFT_ERROR "The admin has just left the room"

#define ROOM_IS_ACTIVE 1

//c'tor
RoomHandler::RoomHandler(RequestHandlerFactory* handlerFactory, LoggedUser user, Room& roomData, bool isAdmin) : m_room(roomData), m_user(user.getUsername()), isAdmin(isAdmin)
{
	this->m_handlerFactory = handlerFactory;
}

/*
get state of the room
input: requestInfo
output: RequestResult - the buffer response and the next handler
*/
RequestResult RoomHandler::getRoomState(RequestInfo requestInfo)
{
	RequestResult requestResult;
	GetRoomStateResponse stateResponse;
	stateResponse.status = SUCCESS;
	stateResponse.answerTimeout = this->m_room.getRoomData().timePerQuestion;
	stateResponse.hasGameBegun = this->m_room.getRoomData().isActive;
	stateResponse.questionCount = this->m_room.getRoomData().questionCount;
	stateResponse.players = this->m_room.getAllUsers();
	if (this->m_room.getAllUsers()[0] != this->m_room.getAdmin().getUsername())
	{
		stateResponse.status = FAILED;
	}
	requestResult.response = JsonResponsePacketSerializer::serializeResponse(stateResponse);

	if (!this->m_room.getAllUsers().empty())
	{

		if (isAdmin) //of the user is admin - the next handler is of admin
		{
			requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createRoomAdminRequestHandler(this->m_user, this->m_room);
		}
		else
		{
			if (this->m_room.getRoomData().isActive == ROOM_IS_ACTIVE) //if the user is admin and the room is \ active - the next handler is of game
			{
				requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createGameRequestHandler(this->m_handlerFactory->getGameManager().getGame(this->m_user), this->m_user);
			}
			else //if the user is admin and the room is not active - the next handler is of member
			{
				requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createRoomMemberRequestHandler(this->m_user, this->m_room);
			}
		}
	}
	else
	{
		requestResult.newHandler = (IRequestHandler*)this->m_handlerFactory->createMenuRequestHandler(this->m_user);
	}
	

	return requestResult;
}