#pragma once

#include <WinSock2.h>
#include <Windows.h>
#include <mutex>
#include <vector>
#include <set>
#include <queue>
#include <map>
#include "Communicator.h"
#include "IDatabase.h"
class Server
{
public:
	~Server();
	void run(); //handle clients
	/*
	Static function in order to create the first instance if needed and get it
	input: the IDataBase class that use the db
	output: the instance of the object
	*/
	static Server* instance(IDatabase* dataAccess);

private:
	/* Private constructor to prevent instancing. */
	Server(IDatabase* dataAccess);
	/* Here will be the instance stored. */
	static Server* server_instance;
	IDatabase* m_database; //database
	Communicator* m_communicator;
	RequestHandlerFactory* m_handlerFactory;

};
