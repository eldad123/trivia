#pragma once
#include "IRequestHandler.h"
#include "LoggedUser.h"
#include "RequestHandlerFactory.h"
#include "RoomManager.h"

class MenuRequestHandler : public IRequestHandler
{
public:
	MenuRequestHandler(std::string username, RequestHandlerFactory* factory); //c'tor
	//d'tor
	virtual ~MenuRequestHandler()
	{

	}
	virtual bool isRequestRelevant(RequestInfo requestInfo); //check if the request is valid in the point of the conecting
	virtual RequestResult handleRequest(RequestInfo requestInfo); //handle the request
	//actions in the menu
	RequestResult signout(RequestInfo requestInfo);
	RequestResult getRooms(RequestInfo requestInfo);
	RequestResult getPlayersInRoom(RequestInfo requestInfo);
	RequestResult getStatistics(RequestInfo requestInfo);
	RequestResult joinRoom(RequestInfo requestInfo);
	RequestResult createRoom(RequestInfo requestInfo);

private:
	LoggedUser m_user; //the user in the menu
	RequestHandlerFactory* m_handlerFactory;

};
