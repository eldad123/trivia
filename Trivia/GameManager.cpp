#include "GameManager.h"
#include <map>
#include <algorithm>
#include <random>

//c'tor
GameManager::GameManager(IDatabase* dataAccess) : m_database(dataAccess)
{
}

/*
create new game
input: the room that the game is in
output: game
*/
Game& GameManager::createGame(Room room)
{

	std::list<Question> questionsLst = m_database->getQuestions(room.getRoomData().questionCount);
	std::vector<Question> questionVec(questionsLst.begin(), questionsLst.end());
	for (auto& question : questionVec)
	{
		std::random_shuffle(question.getPossibleAnswersVec().begin(), question.getPossibleAnswersVec().end());
	}
	std::map<LoggedUser, GameData> players;
	std::vector<LoggedUser> connectUsers;
	for (auto& username : room.getAllUsers())
	{
		players.insert(std::pair<LoggedUser, GameData>(LoggedUser(username), GameData{ questionVec[0], 0,0,0 }));
		connectUsers.push_back(LoggedUser(username));
	}
	this->m_games.push_back(new Game(questionVec, players, connectUsers));
	return *this->m_games.back();
}

/*
get game that an input user is a player in
input: user
output: the room that the user is in
*/
Game& GameManager::getGame(LoggedUser user)
{
	for (auto& game : this->m_games)
	{
		if (game->getPlayers().find(user) != game->getPlayers().end())
		{
			return *game;
		}
	}
	throw std::exception("there is no game that the user is player in");
}

//delete an input game
void GameManager::deleteGame(Game* game)
{
	auto it = std::find(this->m_games.begin(), this->m_games.end(), game);
	if (it != this->m_games.end())
	{
		this->m_games.erase(it);
		delete game;
	}
}


