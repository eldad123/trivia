#pragma once
#include "LoginManager.h"
#include "StatisticManager.h"
#include "RoomManager.h"
#include "GameManager.h"

class LoginRequestHandler;
class MenuRequestHandler;
class RoomAdminRequestHandler;
class RoomMemberRequestHandler;
class GameRequestHandler;

class RequestHandlerFactory
{
public:
	//create handlers
	LoginRequestHandler* createLoginRequestHandler();
	MenuRequestHandler* createMenuRequestHandler(LoggedUser user);
	RoomAdminRequestHandler* createRoomAdminRequestHandler(LoggedUser user, Room& roomData);
	RoomMemberRequestHandler* createRoomMemberRequestHandler(LoggedUser user, Room& roomData);
	GameRequestHandler* createGameRequestHandler(Game& m_game, LoggedUser m_user);

	//getters of mamagers
	LoginManager* getLoginManager();
	StatisticsManager& getStatisticsManager();
	RoomManager& getRoomManager();
	GameManager& getGameManager();

	
	/*
	Static function in order to create the first instance if needed and get it
	input: the IDataBase class that use the db
	output: the instance of the object
	*/
	static RequestHandlerFactory* instance(IDatabase* dataAccess);

private:
	/* Private constructor to prevent instancing. */
	RequestHandlerFactory(IDatabase* dataAccess);
	/* Here will be the instance stored. */
	static RequestHandlerFactory* factory_instance;
	LoginManager* m_loginManager;
	IDatabase* m_database;
	StatisticsManager m_StatisticsManager;
	RoomManager m_roomManager;
	GameManager m_gameManager;

};