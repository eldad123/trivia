#pragma once
#include <iostream>
#include "IDatabase.h"
#include <vector>
#include "LoggedUser.h"
#include <mutex>

class LoginManager
{
public:
	//Actions
	void signup(std::string username, std::string password,std::string email, std::string address, std::string phoneNumber, std::string birthday);
	void login(std::string username, std::string password);
	void logout(std::string username);
	/*
	Static function in order to create the first instance if needed and get it
	input: the IDataBase class that use the db
	output: the instance of the object
	*/
	static LoginManager* instance(IDatabase* dataAccess);

	//check validity
	bool isUsernameValid(std::string username);
	bool isPasswordValid(std::string password);
	bool isEmailValid(std::string email);
	bool isAddressValid(std::string address);
	bool isPhoneNumberValid(std::string phoneNumber);
	bool isBirthdayValid(std::string birthday);

private:
	/* Private constructor to prevent instancing. */
	LoginManager(IDatabase* dataAccess);
	/* Here will be the instance stored. */
	static LoginManager* loginManager_instance;
	IDatabase* m_database;
	std::vector<LoggedUser> m_loggedUsers; //vector of all the logged users
	std::mutex _mtx;
};