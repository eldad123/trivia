#include "JsonRequestPacketDeserializer.h"
#include <fstream>
#include <iostream>
#include "json.hpp"
#include <vector>
#include "RequestsStructs.h"

LoginRequest JsonRequestPacketDeserializer::deserializeLoginRequest(std::vector<unsigned char> buffer)
{
    LoginRequest login;
    nlohmann::json j = nlohmann::json::parse(buffer);
    login.password = j["password"].get<std::string>();
    login.username = j["username"].get<std::string>();
    return login;
}
SignupRequest JsonRequestPacketDeserializer::deserializeSignupRequest(std::vector<unsigned char> buffer)
{
    SignupRequest signUp;
    nlohmann::json j = nlohmann::json::parse(buffer);
    // explicit conversion to string
    signUp.email = j["email"].get<std::string>();
    signUp.password = j["password"].get<std::string>();
    signUp.username = j["username"].get<std::string>();
	signUp.address = j["address"].get<std::string>();
	signUp.birthday = j["birthday"].get<std::string>();
	signUp.phoneNumber = j["phoneNumber"].get<std::string>();

    return signUp;
}

GetPlayersInRoomRequest JsonRequestPacketDeserializer::deserializeGetPlayersRequest(std::vector<unsigned char> buffer)
{
	GetPlayersInRoomRequest getPlayersReq;
	nlohmann::json j = nlohmann::json::parse(buffer);
	// explicit conversion to string
	getPlayersReq.roomId = j["roomId"].get<int>();

	return getPlayersReq;
}

JoinRoomRequest JsonRequestPacketDeserializer::deserializeJoinRoomRequest(std::vector<unsigned char> buffer)
{
	JoinRoomRequest joinRoomReq;
	nlohmann::json j = nlohmann::json::parse(buffer);
	// explicit conversion to string
	joinRoomReq.roomId = j["roomId"].get<unsigned int>();

	return joinRoomReq;
}

CreateRoomRequest JsonRequestPacketDeserializer::deserializeCreateRoomRequest(std::vector<unsigned char> buffer)
{
	CreateRoomRequest createRoomReq;
	nlohmann::json j = nlohmann::json::parse(buffer);
	// explicit conversion to string
	createRoomReq.roomName = j["name"].get<std::string>();
	createRoomReq.maxUsers = j["maxUsers"].get<unsigned int>();
	createRoomReq.questionCount = j["questionCount"].get<unsigned int>();
	createRoomReq.answerTimeout = j["answerTime"].get<unsigned int>();

	return createRoomReq;
}

SubmitAnswerRequest JsonRequestPacketDeserializer::deserializeSubmitAnswerRequest(std::vector<unsigned char> buffer)
{
	SubmitAnswerRequest submitAnswerReq;
	nlohmann::json j = nlohmann::json::parse(buffer);
	submitAnswerReq.answerId = j["answerId"].get<unsigned int>();
	submitAnswerReq.answerTime = j["answerTime"].get<int>();

	return submitAnswerReq;
}

