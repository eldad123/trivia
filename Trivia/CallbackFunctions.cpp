#include "CallbackFunctions.h"
#include "ResponseStructs.h"
#include <iostream>
#include <list>
#include <string>
#include <vector>
#include "Question.h"

#define APOSTROPHE_CODE "&#039;"
#define APOSTROPHE_CODE_LENGTH 6
#define APOSTROPHE "'"

#define QUOTES_CODE "&quot;"
#define QUOTES_CODE_LENGTH 6
#define QUOTES "\""


#define AMPERSAND_CODE "&amp;"
#define AMPERSAND_CODE_LENGTH 5
#define AMPERSAND "&"


/*
This function replace the codes that appear in the answer that recieved from the API
input: string
output: the same string with all the codes already replaced to what they represent
*/
std::string replaceCodesFromTheAPI(std::string string)
{
	while (string.find(QUOTES_CODE) != std::string::npos)
	{
		string.replace(string.find(QUOTES_CODE), QUOTES_CODE_LENGTH, QUOTES);
	}
	while (string.find(APOSTROPHE_CODE) != std::string::npos)
	{
		string.replace(string.find(APOSTROPHE_CODE), APOSTROPHE_CODE_LENGTH, APOSTROPHE);
	}
	while (string.find(AMPERSAND_CODE) != std::string::npos)
	{
		string.replace(string.find(AMPERSAND_CODE), AMPERSAND_CODE_LENGTH, AMPERSAND);
	}
	return string;
}

//get all details about users from db
int getUsernamOfUserFromDB(void* data, int argc, char** argv, char** azColName)
{
	std::list<std::string>* tempLst = (std::list<std::string>*)data;
	std::string username;
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "USERNAME") {
			username = argv[i];
		}
	}
	tempLst->push_back(username);
	return 0;
}

//get total time of user
int callbackTotalTime(void* data, int argc, char** argv, char** azColName)
{
	double* averageTime = (double*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "AVERAGE_TIME")
		{
			*averageTime = std::stod(argv[i]);
		}
	}
	return 0;
}

//get the number of the correct answer
int callbackNumOfCorrectAnswers(void* data, int argc, char** argv, char** azColName)
{
	int* correctAnswers = (int*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "CORRECT_ANSWERS")
		{
			*correctAnswers = std::stoi(argv[i]);
		}
	}
	return 0;
}

//get total answers of user
int callbackNumOfTotalAnswers(void* data, int argc, char** argv, char** azColName)
{
	int* totalAnswers = (int*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "TOTAL_ANSWERS")
		{
			*totalAnswers = std::stoi(argv[i]);
		}
	}
	return 0;
}

//get number of games of user
int callbackNumOfPlayerGames(void* data, int argc, char** argv, char** azColName)
{
	int* totalgames = (int*)data;
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "GAMES_NUMBER")
		{
			*totalgames = std::stoi(argv[i]);
		}
	}
	return 0;
}

//get the top 5 username and their score
int callbackTop5(void* data, int argc, char** argv, char** azColName)
{
	std::vector<TopUser>* top5 = (std::vector<TopUser>*)data;
	double score = 0;
	std::string username = "";
	for (int i = 0; i < argc; i++)
	{
		if (std::string(azColName[i]) == "USERNAME")
		{
			username = argv[i];
		}
		else if (std::string(azColName[i]) == "SCORE")
		{
			score = std::stod(argv[i]);
		}
	}
	top5->push_back(TopUser{ username, score });
	return 0;
}

//get questions (question, 3 wrong answers and one correct answer)
int callBackQuestions(void* data, int argc, char** argv, char** azColName)
{
	std::list<Question>* tempLst = (std::list<Question>*)data;
	std::string question = "";
	std::string correctAnswer = "";
	std::string wrongAnswer1 = "";
	std::string wrongAnswer2 = "";
	std::string wrongAnswer3 = "";
	for (int i = 0; i < argc; i++) {
		if (std::string(azColName[i]) == "question")
		{
			question = replaceCodesFromTheAPI(argv[i]);
		}
		else if (std::string(azColName[i]) == "correct_ans")
		{
			correctAnswer = replaceCodesFromTheAPI(argv[i]);
		}
		else if (std::string(azColName[i]) == "ans2")
		{
			wrongAnswer1 = replaceCodesFromTheAPI(argv[i]);
		}
		else if (std::string(azColName[i]) == "ans3")
		{
			wrongAnswer2 = replaceCodesFromTheAPI(argv[i]);
		}
		else if (std::string(azColName[i]) == "ans4")
		{
			wrongAnswer3 = replaceCodesFromTheAPI(argv[i]);
		}
	}
	tempLst->push_back(Question(question,correctAnswer,wrongAnswer1, wrongAnswer2, wrongAnswer3));
	return 0;
}

