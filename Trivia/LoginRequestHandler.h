#pragma once
#include "IRequestHandler.h"
#include "RequestHandlerFactory.h"

class LoginRequestHandler : public IRequestHandler
{
private:
	RequestHandlerFactory* m_handlerFactory;

public:
	LoginRequestHandler(IDatabase* dataAccess); //c'tor
	//d'tor
	virtual ~LoginRequestHandler()
	{

	}
	virtual bool isRequestRelevant(RequestInfo requestInfo); //check if the request is valid in the point of the conecting
	virtual RequestResult handleRequest(RequestInfo requestInfo); //handle the request
	//actions of login
	RequestResult login(RequestInfo requestInfo);
	RequestResult signup(RequestInfo requestInfo);
};
