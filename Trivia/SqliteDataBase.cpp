#include "SqliteDataBase.h"
#include <list>
#include <sstream> 
#include <exception>
#include "ResponseStructs.h"
#pragma comment(lib, "urlmon.lib")
#include <urlmon.h>
#include "LoggedUser.h"
#define FAILD_BUILD_TABLE 1

#define API_URL "https://opentdb.com/api.php?amount=50&type=multiple"
#define AMOUNT_OF_QUESTIONS 50

#define HTTP_REQUEST_ERROR "error: can not send get request"

#define USERNAME_EXIST_ERROR "username: Used username, change it to something else"
/* Null, because instance will be initialized on demand. */
SqliteDataBase* SqliteDataBase::sql_instance = nullptr;

/*
This function execut sql command
input: sql statement, the call back function, the parameter to send the callback first parameter
output: none
*/
void  SqliteDataBase::executionSqlCommand(std::string sqlStatement, int (*callback)(void*, int, char**, char**), void* param, std::string exeptionInfo)
{
	int res = 0;
	char** errMessage = 0;
	res = sqlite3_exec(this->_db, sqlStatement.c_str(), (*callback), param, errMessage);
	if (res != SQLITE_OK)
	{
		delete errMessage;
		throw std::exception(exeptionInfo.c_str());
	}
	delete errMessage;
}


SqliteDataBase* SqliteDataBase::instance()
{
	if (!sql_instance)
		sql_instance = new SqliteDataBase;
	return sql_instance;
}

//function will close db
void SqliteDataBase::close()
{
	sqlite3_close(this->_db);
	this->_db = nullptr;
}

/*
function will open database
input: none
output: if openning db secceded
*/
bool SqliteDataBase::open()
{
	std::string dbFileName = "TriviaDB.sqlite";
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &(this->_db));
	if (res != SQLITE_OK) {
		this->_db = nullptr;
		std::cout << "Failed to open DB" << std::endl;
		return false;
	}

	this->_error = nullptr;
	if (doesFileExist != 0)
	{
		std::string usersTbl = "CREATE TABLE USERS (USERNAME TEXT NOT NULL PRIMARY KEY, PASSWORD TEXT NOT NULL, ";
		usersTbl += "EMAIL TEXT NOT NULL, ADDRESS TEXT NOT NULL, PHONE_NUMBER TEXT NOT NULL, BIRTHDAY TEXT NOT NULL);";
		int resCreateAlbums = sqlite3_exec(this->_db, usersTbl.c_str(), nullptr, nullptr, _error);
		if (resCreateAlbums != SQLITE_OK)
		{
			std::cout << "Failed CREATE users table." << std::endl;
			return FAILD_BUILD_TABLE;
		}
		std::cout << "creating users table" << std::endl;

		//creating statistics table
		std::string statisticsTbl = "CREATE TABLE STATISTICS (USERNAME TEXT NOT NULL, GAMES_NUMBER INTEGER NOT NULL, CORRECT_ANSWERS INTEGER NOT NULL";
		statisticsTbl += ", TOTAL_ANSWERS INTEGER NOT NULL, AVERAGE_TIME REAL NOT NULL, FOREIGN KEY(USERNAME) REFERENCES USERS(USERNAME));";
		int resStatistics = sqlite3_exec(this->_db, statisticsTbl.c_str(), nullptr, nullptr, _error);
		if (resStatistics != SQLITE_OK)
		{
			std::cout << "Failed CREATE statistics table." << std::endl;
		}
		std::cout << "creating statistics table" << std::endl;
		//add the questions table
		int resAddQuestions = this->createQuestionTableAndAddQuestions();
		if (resAddQuestions == FAILD_BUILD_TABLE)
		{
			return FAILD_BUILD_TABLE;
		}
	}

	return 0;
	
}

/*
function will check if the user exist
input: user name
output: if the user exist
*/
bool SqliteDataBase::doesUserExist(std::string username)
{
	std::string selectUsers = "SELECT USERNAME FROM USERS WHERE USERNAME = '" + username + "';";
	std::list<std::string> usernames;
	this->executionSqlCommand(selectUsers, getUsernamOfUserFromDB, &usernames, "Failed select username user\n");
	if (usernames.empty())
	{
		return false;
	}
	return true;
}


/*
function will check if the password is match to the username
input: password and username
output: if the password is match to the username
*/
bool SqliteDataBase::doesPasswordMatch(std::string username, std::string password)
{
	std::string selectUsers = "SELECT * FROM USERS WHERE USERNAME = '" + username + "' AND PASSWORD = '" + password + "';";
	std::list<std::string> usernames;
	this->executionSqlCommand(selectUsers, getUsernamOfUserFromDB, &usernames, "Failed select user\n");
	if (usernames.empty())
	{
		return false;
	}
	return true;
}

/*
function will create user
input: username, password and email
output: none
*/
void SqliteDataBase::addNewUser(std::string username, std::string password, std::string email, 
	std::string address, std::string phoneNumber, std::string birthday)
{
	if (doesUserExist(username))
	{
		throw std::exception(USERNAME_EXIST_ERROR);
	}
	std::string createUser = "INSERT INTO USERS (USERNAME, PASSWORD, EMAIL, ADDRESS, PHONE_NUMBER, BIRTHDAY) ";
	createUser += "VALUES('" + username + "', '" + password + "', '" + email + "', '" + address + "', '" + phoneNumber + "', '" + birthday + "'); ";
	this->executionSqlCommand(createUser, nullptr, nullptr, "failed create user\n");

	std::string createStatistics = "INSERT INTO STATISTICS (USERNAME, GAMES_NUMBER, CORRECT_ANSWERS, TOTAL_ANSWERS, AVERAGE_TIME) ";
	createStatistics += "VALUES('" + username + "', " + std::to_string(0) + ", " + std::to_string(0) + ", " + std::to_string(0) + ", " + std::to_string(1) + "); ";
	this->executionSqlCommand(createStatistics, nullptr, nullptr, "failed create statistics\n");
}

/*
get number of average time
input: username
output: number of average time
*/
double SqliteDataBase::getPlayerAverageAnswerTime(std::string username)
{
	std::string sqlStatment = "SELECT AVERAGE_TIME FROM STATISTICS WHERE username = '" + username + "';";
	double averageTime = 0;
	this->executionSqlCommand(sqlStatment, callbackTotalTime, &averageTime, "Failed get average time\n");
	return averageTime;
}

/*
get number of correct answers
input: username
output: number of correct answers
*/
int SqliteDataBase::getNumOfCorrectAnswers(std::string username)
{
	std::string sqlStatment = "SELECT CORRECT_ANSWERS  FROM STATISTICS WHERE USERNAME = '" + username + "';";
	int correctAnswers = 0;
	this->executionSqlCommand(sqlStatment, callbackNumOfCorrectAnswers, &correctAnswers, "Failed get correct answers\n");
	return correctAnswers;
}

/*
get number of total answers
input: username
output: number of total answers
*/
int SqliteDataBase::getNumOfTotalAnswers(std::string username)
{
	std::string sqlStatment = "SELECT TOTAL_ANSWERS FROM STATISTICS WHERE USERNAME = '" + username + "';";
	int totalAnswers = 0;
	this->executionSqlCommand(sqlStatment, callbackNumOfTotalAnswers, &totalAnswers, "Failed get total answers\n");
	return totalAnswers;
}

/*
get number of player games
input: username
output: number of player games
*/
int SqliteDataBase::getNumOfPlayerGames(std::string username)
{
	std::string sqlStatment = "SELECT GAMES_NUMBER FROM STATISTICS WHERE USERNAME = '" + username + "';";
	int playerGames = 0;
	this->executionSqlCommand(sqlStatment,callbackNumOfPlayerGames, &playerGames, "Failed get player games\n");
	return playerGames;
}

//get vector of the top 5
std::vector<TopUser> SqliteDataBase::getTopFiveScores()
{
	std::string sqlStatment = "SELECT username,(CORRECT_ANSWERS * 1.0 / AVERAGE_TIME) as SCORE FROM STATISTICS ORDER by(CORRECT_ANSWERS * 1.0 / AVERAGE_TIME) DESC  LIMIT 5;";
	std::vector<TopUser> top5;
	this->executionSqlCommand(sqlStatment, callbackTop5, &top5, "Failed get player games\n");
	return top5;
}

/*
This function get an sql insert command for a question
input: the json that recieved from the API, the index of the questions
output: the insert command
*/
std::string SqliteDataBase::getInsertCommand(nlohmann::json data, int questionIndex)
{
	std::string sqlCommand = "INSERT INTO QUESTIONS (question,correct_ans,ans2,ans3,ans4) VALUES(";
	sqlCommand += "'" + data[questionIndex]["question"].get<std::string>() + "',";
	sqlCommand += "'" + data[questionIndex]["correct_answer"].get<std::string>() + "',";
	for (int i = 0; i < 3; i++)
	{
		if (data[questionIndex]["incorrect_answers"].size() <= i)
		{
			sqlCommand += "'none',";
		}
		else
		{
			sqlCommand += "'" + data[questionIndex]["incorrect_answers"][i].get<std::string>() + "',";
		}
	}
	sqlCommand.pop_back();
	sqlCommand += ");";
	return sqlCommand;
}

/*
This function get the json that recieved from the API
input: none
output: the json that recieved from the API
*/
std::string SqliteDataBase::getJsonOfQuestions()
{
	IStream* stream;
	//Also works with https URL's - unsure about the extent of SSL support though.
	HRESULT result = URLOpenBlockingStream(0, (LPCSTR)API_URL, &stream, 0, 0);
	if (result != 0)
	{
		throw std::exception(HTTP_REQUEST_ERROR);
	}
	char buffer[100];
	unsigned long bytesRead;
	std::stringstream ss;
	stream->Read(buffer, 100, &bytesRead);
	while (bytesRead > 0U)
	{
		ss.write(buffer, (long long)bytesRead);
		stream->Read(buffer, 100, &bytesRead);
	}
	stream->Release();
	return ss.str();
}

/*
This function add the question to the db
input: the sql that recieved from the API
output: 0 if all the questions entered, 1 - if not all the questions entered
*/
bool SqliteDataBase::addQuestionsToDB(std::string jsonString)
{
	nlohmann::json j = nlohmann::json::parse(jsonString);
	for (int i = 0; i < AMOUNT_OF_QUESTIONS; i++)
	{
		int resAddQuestion = sqlite3_exec(this->_db, this->getInsertCommand(j["results"], i).c_str(), nullptr, nullptr, _error);
		if (resAddQuestion != SQLITE_OK)
		{
			std::cout << this->getInsertCommand(j["results"], i) << std::endl;
			return  FAILD_BUILD_TABLE;
		}
	}
	return 0;
}

/*
This function create the table and add the questions to the db
input: none
output: 0 - if the table created and the questions added, 1 - if something failed 
*/
bool SqliteDataBase::createQuestionTableAndAddQuestions()
{
	std::string questionsTable = "CREATE TABLE QUESTIONS ( question_id integer NOT NULL PRIMARY KEY AUTOINCREMENT, question text NOT NULL,";
	questionsTable += "correct_ans text NOT NULL, ans2 text NOT NULL, ans3 text NOT NULL, ans4 text NOT NULL );";
	int resCreateAlbums = sqlite3_exec(this->_db, questionsTable.c_str(), nullptr, nullptr, _error);
	if (resCreateAlbums != SQLITE_OK)
	{
		std::cout << "Failed Create Question Table." << std::endl;
		return  FAILD_BUILD_TABLE;
	}
	std::cout << "creating Question table" << std::endl;
	std::string jsonResponse = this->getJsonOfQuestions();
	return this->addQuestionsToDB(jsonResponse);
}

/*
get random questions
input: number of random questions
output: list of of random questions
*/
std::list<Question> SqliteDataBase::getQuestions(int numberOfQuestions)
{
	std::list<Question> questions;
	std::string selectUsers = "SELECT * FROM QUESTIONS ORDER BY RANDOM() LIMIT " + std::to_string(numberOfQuestions) + ";";
	this->executionSqlCommand(selectUsers, callBackQuestions, &questions, "Failed select user\n");
	return questions;
}


void SqliteDataBase::updateStatistics(GameData gameData, LoggedUser user)
{
	int totalAnswers = gameData.correctAnswerCount + gameData.wrongAnswerCount;
	int totalTime = gameData.averangeAnswerTime * totalAnswers;
	std::string sqlStatment = "BEGIN;";
	this->executionSqlCommand(sqlStatment, nullptr, nullptr, "Failed BEGIN\n");

	//update correct answers count
	sqlStatment = "UPDATE STATISTICS SET CORRECT_ANSWERS = ((SELECT CORRECT_ANSWERS FROM STATISTICS WHERE USERNAME = '" + user.getUsername() + "') +";
	sqlStatment += std::to_string(gameData.correctAnswerCount) + ") WHERE USERNAME = '" + user.getUsername() + "';";
	this->executionSqlCommand(sqlStatment, nullptr, nullptr, "Failed END\n");

	//update total answers count
	sqlStatment = "UPDATE STATISTICS SET TOTAL_ANSWERS = ((SELECT TOTAL_ANSWERS FROM STATISTICS WHERE USERNAME = '" + user.getUsername() + "') +";
	sqlStatment += std::to_string(totalAnswers) + ") WHERE USERNAME = '" + user.getUsername() + "';";
	this->executionSqlCommand(sqlStatment, nullptr, nullptr, "Failed END\n");

	//update total games count
	sqlStatment = "UPDATE STATISTICS SET GAMES_NUMBER = ((SELECT GAMES_NUMBER FROM STATISTICS WHERE USERNAME = '" + user.getUsername() + "') +";
	sqlStatment += std::to_string(1) + ") WHERE USERNAME = '" + user.getUsername() + "';";
	this->executionSqlCommand(sqlStatment, nullptr, nullptr, "Failed END\n");

	//update average time
	sqlStatment = "UPDATE STATISTICS SET AVERAGE_TIME = (((((SELECT AVERAGE_TIME FROM STATISTICS WHERE USERNAME = '" + user.getUsername() + "') * ";
	sqlStatment += " (SELECT TOTAL_ANSWERS FROM STATISTICS WHERE USERNAME = '" + user.getUsername() + "')) + " + std::to_string(totalTime) + ")) / ";
	sqlStatment += "((SELECT TOTAL_ANSWERS FROM STATISTICS WHERE USERNAME = '" + user.getUsername() + "') + " + std::to_string(totalAnswers) + ")) WHERE USERNAME = '" + user.getUsername() + "';";
	this->executionSqlCommand(sqlStatment, nullptr, nullptr, "Failed Update averageTime\n");

	sqlStatment = "END;";
	this->executionSqlCommand(sqlStatment, nullptr, nullptr, "Failed END\n");
}