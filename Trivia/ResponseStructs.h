#pragma once

#include <iostream>
#include <vector>
#include <map>
#include "DataStructs.h"

typedef struct ErrorResponse {
	std::string message;
} ErrorResponse;

typedef struct LoginResponse {
	unsigned int status;
	std::string err;
} LoginResponse;


typedef struct SignupResponse {
	unsigned int status;
	std::string err;
} SignupResponse;

typedef struct LogoutResponse {
	unsigned int status;
	std::string err;
} LogoutResponse;


typedef struct GetRoomsResponse{
	unsigned int status;
	std::vector<RoomData> rooms;
} GetRoomsResponse;


typedef struct GetPlayersInRoomResponse {
    std::vector<std::string> players;
} GetPlayersInRoomResponse;


typedef struct GetStatisticsResponse {
	unsigned int status;
	std::vector<std::vector<std::string>> statistics;

} GetStatisticsResponse;


typedef struct JoinRoomResponse {
	int status;
} JoinRoomResponse;

typedef struct CreateRoomResponse {
	unsigned int status;
} CreateRoomResponse;

typedef struct TopUser {
	std::string username;
	double score;
} TopUser;


typedef struct CloseRoomResponse {
	unsigned int status;
	std::string err;
} CloseRoomResponse;

typedef struct StartGameResponse {
	unsigned int status;
	std::string err;
} StartGameResponse;

typedef struct GetRoomStateResponse {
	int status;
	bool hasGameBegun;
	std::vector<std::string> players;
	int questionCount;
	int answerTimeout;
	std::string err;
} GetRoomStateResponse;

typedef struct LeaveRoomResponse {
	unsigned int status;
	std::string err;
} LeaveRoomResponse;

typedef struct GetQuestionResponse {
	unsigned int status;
	std::string question;
	std::map<int, std::string> answers;
} GetQuestionResponse;

typedef struct SubmitAnswerResponse {
	unsigned int status;
	int correctAnswerId;
} SubmitAnswerResponse;

typedef struct PlayerResults {
	std::string username;
	unsigned int correctAnswerCount;
	unsigned int wrongAnswerCount;
	double averageAnswerTime;
} PlayerResults;

typedef struct GetGameResultsResponse {
	unsigned int status;
	std::vector<PlayerResults> results;
} GetGameResultsResponse;

typedef struct LeaveGameResponse {
	unsigned int status;
	std::string err;
} LeaveGameResponse;