#include "JsonResponsePacketSerializer.h"
#include "Room.h"

#define FAILED 0
#define SUCCESS 1

#define SIGN_UP 0
#define LOGIN_CODE 1
#define GET_STATS_CODE 8
#define INDEX_OF_CORRECT_ANSWERS 0
#define ERROR_CODE 2
#define LOGOUT_CODE 3
#define CREATE_ROOM_CODE 4
#define JOIN_ROOM_CODE 5
#define GET_ROOMS_CODE 6
#define GET_PLAYERS_CODE 7
#define GET_STATS_CODE 8
#define CLOSE_ROOM_CODE 9
#define START_GAME_CODE 10
#define GET_ROOM_STATE_CODE 11
#define LEAVE_ROOM_CODE 12
#define GET_RESULTS_CODE 13
#define SUBMIT_ANSWER_CODE 14
#define GET_QUESTION_CODE 15
#define LEAVE_GAME_CODE 16

#define INDEX_STATISTICS 0
#define INDEX_OF_CORRECT_ANSWERS 0
#define INDEX_OF_PLAYER_GAMES 1
#define INDEX_OF_TOTAL_ANSWERS 2
#define INDEX_OF_AVERAGE_TIME 3
#define INDEX_OF_USERNAME 0
#define INDEX_OF_SCORE 1

std::vector<unsigned char> JsonResponsePacketSerializer::pushProtocolParamsToVector(int code, int dataLen, std::string data)
{
	std::vector<unsigned char> res;

	res.push_back(char(code));
	
	//create place in the vector for the 4 bytes you want to enter
	res.resize(5);
	memcpy(&res[1], &dataLen, sizeof(dataLen));
	std::copy(data.begin(), data.end(), std::back_inserter(res));
	return res;
}

//serialize signup response
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SignupResponse signup)
{
	nlohmann::json j = { {"status", signup.status}};
	if (signup.err != "")
	{
		j["error"] = signup.err;
	}
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(SIGN_UP, length, data);
}

//serialize login response
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LoginResponse login)
{
	nlohmann::json j = { {"status", login.status},  };
	if (login.err != "")
	{
		j["error"] = login.err;
	}

	std::string data = j.dump();
	int length = data.size();
	
	return pushProtocolParamsToVector(LOGIN_CODE, length, data);
}

//serialize error response
std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(ErrorResponse err)
{
	nlohmann::json j = { {"message", err.message} };
	std::string data = j.dump();
	int length = data.size();
	return pushProtocolParamsToVector(ERROR_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LogoutResponse logout)
{
	nlohmann::json j = { {"status", logout.status} };
	if (logout.err != "")
	{
		j["error"] = logout.err;
	}
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(LOGOUT_CODE, length, data);
}



std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CreateRoomResponse createRoom)
{
	nlohmann::json j = { {"status", createRoom.status} };
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(CREATE_ROOM_CODE, length, data);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(JoinRoomResponse joinRoom)
{
	nlohmann::json j = { {"status", joinRoom.status} };
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(JOIN_ROOM_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomsResponse getRooms)
{
	std::string allRoomsStr;
	std::vector<nlohmann::json> vec;

	for (int i = 0; i < getRooms.rooms.size(); i++)
	{
		nlohmann::json jRoom = { {"name", getRooms.rooms[i].name},
		{"id", getRooms.rooms[i].id},
		{"questionsCount", getRooms.rooms[i].questionCount},
		{"maxPlayers", getRooms.rooms[i].maxPlayers},
		{"timePerQuestion", getRooms.rooms[i].timePerQuestion},
		{"isActive", getRooms.rooms[i].isActive} };

		vec.push_back(jRoom);

	}
	

	nlohmann::json j = { {"Rooms", vec} };
	std::string data = j.dump();

	int length = data.size();

	return pushProtocolParamsToVector(GET_ROOMS_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetPlayersInRoomResponse getPlayers)
{
	std::string allPlayers;
	for (int i = 0; i < getPlayers.players.size(); i++)
	{
		allPlayers += getPlayers.players[i];
		allPlayers += ", ";
	}
	if (allPlayers != "")
	{
		allPlayers.pop_back();
	}
	if (allPlayers != "")
	{
		allPlayers.pop_back();
	}

	nlohmann::json j = { {"Players", allPlayers} };
	std::string data = j.dump();

	int length = data.size();

	return pushProtocolParamsToVector(GET_PLAYERS_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetStatisticsResponse getStats)
{
	std::vector<unsigned char> res;
	std::vector<nlohmann::json> topScores;
	int length = 0;
	nlohmann::json j = { {"status", getStats.status} };
	j["UserStatistics"] = { {"currect answers", getStats.statistics[0][INDEX_OF_CORRECT_ANSWERS] },
	{ "games number", getStats.statistics[0][INDEX_OF_PLAYER_GAMES] }, {"total answers", getStats.statistics[0][INDEX_OF_TOTAL_ANSWERS] } ,
	{"average time", getStats.statistics[0][INDEX_OF_AVERAGE_TIME] } };

	for (int i = 1; i < getStats.statistics.size(); i++)
	{
		topScores.push_back({ { "username", getStats.statistics[i][INDEX_OF_USERNAME] }, { "score", getStats.statistics[i][INDEX_OF_SCORE] } });
	}
	j.push_back({ "BestScores", topScores });
	std::string data = j.dump();
	length = data.size();
	return pushProtocolParamsToVector(GET_STATS_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(CloseRoomResponse closeRoom)
{
	nlohmann::json j = { {"status", closeRoom.status} };
	if (closeRoom.err != "")
	{
		j["error"] = closeRoom.err;
	}
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(CLOSE_ROOM_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(StartGameResponse startGame)
{
	nlohmann::json j = { {"status", startGame.status} };
	if (startGame.err != "")
	{
		j["error"] = startGame.err;
	}
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(START_GAME_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetRoomStateResponse gameState)
{
	nlohmann::json j = { {"status", gameState.status} };
	if (gameState.status != FAILED)
	{
		j["hasGameBegun"] = gameState.hasGameBegun;
		j["players"] = gameState.players;
		j["questionCount"] = gameState.questionCount;
		j["answerTimeOut"] = gameState.answerTimeout;
	}

	if (gameState.err != "")
	{
		j["error"] = gameState.err;
	}
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(GET_ROOM_STATE_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveRoomResponse leaveRoom)
{
	nlohmann::json j = { {"status", leaveRoom.status} };
	if (leaveRoom.err != "")
	{
		j["error"] = leaveRoom.err;
	}
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(LEAVE_ROOM_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetGameResultsResponse getResults)
{
	nlohmann::json j = { {"status", getResults.status} };
	std::vector<nlohmann::json> vec;
	if (getResults.status == SUCCESS)
	{
		for (auto& player : getResults.results)
		{
			nlohmann::json jPlayer = { {"username", player.username },
			{"correctAnswerCount", player.correctAnswerCount },
			{"wrongAnswerCount", player.wrongAnswerCount},
			{"averageAnswerTime", player.averageAnswerTime} };

			vec.push_back(jPlayer);
		}
		j["results"] = vec;
	}
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(GET_RESULTS_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(SubmitAnswerResponse submitAnswer)
{
	nlohmann::json j = { {"status", submitAnswer.status}, {"correctAnswerId", submitAnswer.correctAnswerId} };
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(SUBMIT_ANSWER_CODE, length, data);
}


std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(GetQuestionResponse getQuestion)
{
	nlohmann::json j = { {"status", getQuestion.status}, {"question", getQuestion.question} };
	std::vector<nlohmann::json> vec;
	if (getQuestion.status == SUCCESS)
	{
		for (auto& answer : getQuestion.answers)
		{
			nlohmann::json jQuestion = { {"number", answer.first},
				{"answer", answer.second } };
			vec.push_back(jQuestion);
		}
		j["answers"] = vec;
	}

	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(GET_QUESTION_CODE, length, data);
}

std::vector<unsigned char> JsonResponsePacketSerializer::serializeResponse(LeaveGameResponse leaveGame)
{
	nlohmann::json j = { {"status", leaveGame.status} };
	if (leaveGame.err != "")
	{
		j["error"] = leaveGame.err;
	}
	std::string data = j.dump();
	int length = data.size();

	return pushProtocolParamsToVector(LEAVE_GAME_CODE, length, data);
}
