#pragma once
#include "IRequestHandler.h"
#include "Room.h"
#include "LoggedUser.h"
#include "RoomManager.h"
#include "RequestHandlerFactory.h"
#include "RequestsStructs.h"
#include "RoomHandler.h"
#include <iostream>
class RoomAdminRequestHandler : IRequestHandler, RoomHandler
{
public:
	RoomAdminRequestHandler(RequestHandlerFactory* m_handlerFactory, LoggedUser user, Room& roomData); //c'tor
	virtual bool isRequestRelevant(RequestInfo requestInfo); //check if the request is valid in the point of the conecting
	virtual RequestResult handleRequest(RequestInfo requestInfo); //handle the request
	//actions of room's admin
	RequestResult closeRoom(RequestInfo requestInfo);
	RequestResult startGame(RequestInfo requestInfo);

};