#include "Game.h"
#include "IDatabase.h"
#include "SqliteDataBase.h"
#define INDEX_OF_TIME_OVER -1

//c'tor
Game::Game(std::vector<Question> questions, std::map<LoggedUser, GameData> players, std::vector<LoggedUser> connect_users)
{
	this->m_questions = questions;
	this->m_players = players;
	this->connected_uers = connect_users;
}

/*
get the current question of user
input: user
output: his current question
*/
Question Game::getQuestionForUser(LoggedUser user)
{
	std::map<LoggedUser, GameData>::iterator it = this->m_players.find(user);
	Question currQuestion = it->second.currentQuestion;
	//int totalNumOfQuestions = this->m_questions.size();
	for (int i = 0; i < this->m_questions.size(); i++)
	{
		if (this->m_questions[i] == currQuestion)
		{
			return this->m_questions[i];
		}
	}
	throw std::exception("you have finished");
}

/*
Get current question index in the question vector
input: the question
output: question index in the question vector
*/
int Game::GetCurrentQuestionIndex(Question q)
{
	for (int i = 0; i < this->m_questions.size(); i++)
	{
		if (this->m_questions[i] == q)
			return i;
	}
}

std::vector<LoggedUser> Game::GetConnectedPlayers() const
{
	return this->connected_uers;
}

/*
submit the answer
input: user and answer index in the vector
output: none
*/
void Game::submitAnswer(LoggedUser user, int userAnsIndex)
{
	std::map<LoggedUser, GameData>::iterator itUser = this->m_players.find(user);
	if (userAnsIndex == INDEX_OF_TIME_OVER)
	{
		itUser->second.wrongAnswerCount++;

		if (this->GetCurrentQuestionIndex(itUser->second.currentQuestion) != this->m_questions.size() - 1)
			itUser->second.currentQuestion = this->m_questions[this->GetCurrentQuestionIndex(itUser->second.currentQuestion) + 1];
		throw std::exception("Time over");
	}
	if (itUser != this->m_players.end())
	{
		auto itQuestionWithCorrectAns = std::find(this->m_questions.begin(), this->m_questions.end(), itUser->second.currentQuestion);
		std::string userAns = itQuestionWithCorrectAns->getPossibleAnswersVec()[userAnsIndex];
		if (userAns == itQuestionWithCorrectAns->getCorrectAnswer())
		{
			itUser->second.correctAnswerCount++;
		}
		else
		{
			itUser->second.wrongAnswerCount++;
		}

		if (this->GetCurrentQuestionIndex(itUser->second.currentQuestion) != this->m_questions.size() - 1)
			itUser->second.currentQuestion = this->m_questions[this->GetCurrentQuestionIndex(itUser->second.currentQuestion) + 1];
	}
	else
	{
		throw std::exception("there is no user like you entered");
	}
}

//function will remove an input user from the map
void Game::removePlayer(LoggedUser user)
{
	auto it = std::find(this->connected_uers.begin(), this->connected_uers.end(), user);
	if (it != this->connected_uers.end())
	{
		if (this->m_players.find(user)->second.averangeAnswerTime == 0)
		{
			this->m_players.erase(user);
		}
		this->connected_uers.erase(it);
	}
}

//operator==
bool Game::operator==(const Game& other) const
{
	if (other.connected_uers.empty() || this->connected_uers.empty())
	{
		return false;
	}
	std::string oneOfTheUsers = other.connected_uers.back().getUsername();
	for (auto& user : this->connected_uers)
	{
		if (oneOfTheUsers == user.getUsername())
		{
			return true;
		}
	}
	return false;
}

//getter of the players map
std::map<LoggedUser, GameData>& Game::getPlayers()
{
	return this->m_players;
}

//getter of the questions vector
std::vector<Question> Game::getQuestions() const
{
	return this->m_questions;
}


