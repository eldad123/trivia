#include "RoomManager.h"
#define DID_NOT_FIND_ROOM_ERROR "No room has this id"

/*
create a room
input: user who created the room (the admin) and the room data
output: npnt
*/
void RoomManager::createRoom(LoggedUser userCreatedRoom, RoomData roomData)
{
	roomData.id = this->_lastRoomId;
	this->_lastRoomId ++;
	this->m_rooms.insert(std::pair<int, Room*>(roomData.id, new Room(roomData, userCreatedRoom)));

	for (std::map<int, Room*>::iterator room = this->m_rooms.begin(); room != m_rooms.end(); ++room)
	{
		if(room->first == this->_lastRoomId - 1)
		{
			room->second->addUser(userCreatedRoom); //add the admin
		}
	}
}

//delete room by input room id
void RoomManager::deleteRoom(int ID)
{
	std::map<int, Room*>::iterator it;
	it = this->m_rooms.find(ID);
	if (it != this->m_rooms.end())
	{
		delete it->second;
		this->m_rooms.erase(it);
	}
	else
	{
		throw std::exception(DID_NOT_FIND_ROOM_ERROR);
	}
}

//get room by input room id
Room& RoomManager::getRoomById(int ID)
{
	return *this->m_rooms.find(ID)->second;
}

//get room state room by input room id
unsigned int RoomManager::getRoomState(int ID)
{
	std::map<int, Room*>::iterator it;

	it = m_rooms.find(ID);
	if (it != m_rooms.end())
	{
		return it->second->getRoomData().isActive;
	}
	else
	{
		throw std::exception(DID_NOT_FIND_ROOM_ERROR);
	}
}

//get all the rooms
//output: vector pf all the rooms
std::vector<Room> RoomManager::getRooms()
{
	std::vector<Room> allRooms;

	for (auto& room : m_rooms)
	{
		allRooms.push_back(*room.second);
	}

	return allRooms;
}
