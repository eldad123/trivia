#pragma once
#include <iostream>
#include <vector>
#include "DataStructs.h"
#include "LoggedUser.h"
class Room
{
public:
	Room(RoomData roomData, LoggedUser admin);//c'tor
	void addUser(LoggedUser user); //add user to the room
	void removeUser(LoggedUser user); //remove user from the room
	std::vector <std::string> getAllUsers(); //get users in the room
	RoomData& getRoomData(); //get the room data
	LoggedUser getAdmin();

private:
	LoggedUser m_admin;
	RoomData m_metadata; //room data
	std::vector <LoggedUser> m_users; //users in the room
};