import socket
import json
import struct
#const codes
SIGNUP_CODE = 0
LOGIN_CODE = 1
#const responses
SUCCESS = 1
FAIL = 0
#const options
LOGIN_OPTION = "1"
SIGNUP_OPTION = "2"
def create_sock():
    """
    create socket
    :return: the socket or false if faild to connect
    """
    #information about the server
    SERVER_IP = "127.0.0.1"
    SERVER_PORT = 3000
    try:
        #connect to the server
        sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        server_address = (SERVER_IP, SERVER_PORT)
        sock.connect(server_address)
    except Exception as e:
            print("option=Error&info=", e)#print the error
            return False#faild to connect
    return sock


def recieveMessageFromServer(clientSock, size):
    message = clientSock.recv(size)
    return message


def sendMessageToServer(clientSock, messageCode, jsonMessage):
    """
    :param client_msg: the message of the client
    :param sock: the socket
    get response from the server and send request
    :return: true if send and receive successfully or false if faild
    """
    try:
        #send message to the client and get message from him
        clientSock.sendall(struct.pack('>b', messageCode))
        #clientSock.sendall(struct.pack('B', messageCode))
        clientSock.sendall(struct.pack('<i',len(bytes(json.dumps(jsonMessage),encoding="utf-8"))))
        #clientSock.sendall(struct.pack('i',len(bytes(json.dumps(jsonMessage),encoding="utf-8"))))
        #clientSock.sendall(bytes(json.dumps(jsonMessage),encoding="utf-8"))
        clientSock.sendall(bytes(json.dumps(jsonMessage),encoding="utf-8"))
    except Exception as e:
        print(e)
        return False
    return True

#receive the message and divide it to the 3 parts
def manageReceivedMessage(sock):
    #recieve the message from client
    code = struct.unpack("B",recieveMessageFromServer(sock,1))[0]
    length = struct.unpack("i",recieveMessageFromServer(sock,4))[0]
    jsonMessage = recieveMessageFromServer(sock,length).decode("utf-8")
    data = json.loads(jsonMessage)
    return [code, length, data]


def signup(sock):
    name = input("enter your username: ")
    password = input("enter your password: ")
    email = input("enter your email: ")
    address = input("enter your address: ")
    phoneNumber = input("enter your phone number: ")
    birthday = input("enter your birthday: ")
    sendMessageToServer(sock, SIGNUP_CODE, {"username" : name, "password": password, "email": email, "address": address,
        "phoneNumber": phoneNumber, "birthday": birthday})
    print("sent signup message")
    message_details = manageReceivedMessage(sock)
    print(message_details)
    if (message_details[2]["status"] == SUCCESS):
        return True
    return False
    
def login(sock):
    name = input("enter your username: ")
    password = input("enter your password: ")
    sendMessageToServer(sock, LOGIN_CODE, {"username" : name, "password": password})
    print("sent login message")
    message_details = manageReceivedMessage(sock)
    print(message_details)
    if (message_details[2]["status"] == SUCCESS):
        return True
    return False
    
def main():
    sock = create_sock()
    if sock != False :#if the client connect successfully to the server
        
        try:
            startMessage = recieveMessageFromServer(sock,5).decode()
            if(startMessage == "Hello"):
                while True:
                    try:
                        choice = input("1 - login \n2 - signup \nchoose an option: ")
                        if(choice == LOGIN_OPTION):
                            login(sock)
                        elif(choice == SIGNUP_OPTION):
                            signup(sock)
                    except Exception as e:
                        print(e)
                        sock.close()
        except Exception as e:
                        print(e)
                        sock.close()

if __name__ == "__main__":
    main()
