﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;
using TriviaClient.DataClasses;
using TriviaClient.ResponseClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for StartGame.xaml
    /// </summary>
    public partial class StartGame : Page
    {
        public string username;
        public string roomName;
        private int questionsCount;
        private int timePerQuestions;
        private BackgroundWorker background_worker = new BackgroundWorker();

        public StartGame(string roomName, string username, int questionsCount, int timePerQuestions)
        {
            InitializeComponent();
            this.username = username;
            this.roomName = roomName;
            this.roomNameBlock.Text = this.roomName;
            this.timePerQuestions = timePerQuestions;
            this.questionsCount = questionsCount;

            //initialize the background worker
            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;
            background_worker.DoWork += background_worker_DoWork;
            background_worker.ProgressChanged += background_worker_ProgressChanged;
            background_worker.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync();
        }

        //Activate when the background worker is finished or canceled
        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            
        }

        //Activate when reporting on proggress and init the users table
        void background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            List<User> users = new List<User>();
            bool isFirst = true;
            //if the get room state is faild
            if (((GetRoomStateResponse)e.UserState).status == Constants.FAILD_STATUS)
            {
                throw new Exception("cant access to room");
            }
            //init the table
            foreach (string user in ((GetRoomStateResponse)e.UserState).players)
            {
                if (isFirst)
                {
                    users.Add(new User(user + " ------- Admin"));
                    isFirst = false;
                }
                else
                {
                    users.Add(new User(user));
                }
            }
            dataGrid.ItemsSource = users;
        }

        //the thread that run in the background
        void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int i = 0;
            try
            {
                while (true)
                {
                    //check if the thread was canceled
                    if (background_worker.CancellationPending)
                    {
                        e.Cancel = true;
                        break;
                    }
                    //send get room state request to the server and get response
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.GET_ROOM_STATE_CODE));
                    byte[] jsonResponseForRooms = Communicator.getMessageFromServer();
                    GetRoomStateResponse getState = Deserializer.DeserializerGetRoomState(jsonResponseForRooms);
                    //if the request is not valid at this point
                    if(getState.err == "The message in not valid at this point")
                    {
                        throw new Exception("This option is invalid at this point");
                    }
                    //active ReportProggress in order to update the users table
                    background_worker.ReportProgress(i, getState);
                    Thread.Sleep(3000);//make the thread wait for 3 seconds
                    i++;
                }
            }
            catch (Exception err)//if the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                e.Cancel = true;//cancel the background worker
            }
        }

        //This function activate when the user press close Button and it close the room
        private void Close_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                background_worker.CancelAsync();
                //send close room request to the server and get response
                Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.CLOSE_ROOM_CODE));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                LeaveRoomResponse leaveRoom = Deserializer.DeserializerLeaveRoomResponse(jsonResponse);
                //if this option is invalid at this point
                if (leaveRoom.errMessage == "The message in not valid at this point")
                {
                    MessageBox.Show("This option is invalid at this point", "Error");
                }
                else if (leaveRoom.status == Constants.SUCCESS_STATUS)//if the room was closed succesfully
                {
                    this.NavigationService.Navigate(new MenuPage(this.username));
                }
            }
            catch (Exception err)//if the server shutdown
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                background_worker.CancelAsync();//cancel the background worker

            }

        }

        //This function activate when the user press start button. It start the game
        private void Start_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //send start room request to the server and get response
                Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.START_GAME_CODE));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                background_worker.CancelAsync();//cancel the backgroundWorker
                //move to the game page
                this.NavigationService.Navigate(new GamePage(this.roomName, this.username, this.questionsCount, this.timePerQuestions));
            }
            catch (Exception err)//if the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                background_worker.CancelAsync();//cancel the backgroundWorker

            }

        }
    }
}
