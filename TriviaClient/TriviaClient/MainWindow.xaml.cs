﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using TriviaClient.classes;
using System.ComponentModel;
using TriviaClient.ResponseClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            //make the window fit to the size of all the [ages
            // Manually alter window height and width
            this.SizeToContent = SizeToContent.Manual;
            // Automatically resize width relative to content
            this.SizeToContent = SizeToContent.Width;
            // Automatically resize height relative to content
            this.SizeToContent = SizeToContent.Height;
            // Automatically resize height and width relative to content
            this.SizeToContent = SizeToContent.WidthAndHeight;
            //trying to make a connection in a loop until the user don't want anymore
            while (true)
            {
                bool tryAgain = false;//save if the user want to try to connect again
                bool succeed = Communicator.StartClient();//try to connect to the server and return if succeed or not
                //if the connection faild
                if (!succeed)
                {
                    //ask the user fot decision
                    MessageBoxResult result = MessageBox.Show("There was an error to create connection.\nWould you like to try again?", "Error", MessageBoxButton.YesNo);
                    //check what option the user choose 
                    switch (result)
                    {
                        //if the user choose yes and want to tru again
                        case MessageBoxResult.Yes:
                            tryAgain = true;
                            break;
                        //if the user choose no and do not want to try to connect again
                        case MessageBoxResult.No:
                            //shut down the window
                            Application.Current.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                            Application.Current.Shutdown();
                            break;
                    }
                }
                if(!tryAgain)//if the user do not want to try again or the client connected to the server
                {
                    break;
                }
            }
            //navigate to the next page
            Main.Navigate(new Signin());
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            try
            {
                if (Main.Content is StartGame)
                {
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.CLOSE_ROOM_CODE));
                    byte[] jsonResponse = Communicator.getMessageFromServer();
                    Communicator.Logout();
                }
                else if(Main.Content is WaitForGameToStart)
                {
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.LEAVE_ROOM_CODE));
                    byte[] jsonResponse = Communicator.getMessageFromServer();
                    LeaveRoomResponse leaveRoom = Deserializer.DeserializerLeaveRoomResponse(jsonResponse);
                    Communicator.Logout();
                }
                else if(Main.Content is GameResults || Main.Content is GamePage)
                {
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.LEAVE_GAME_CODE));
                    byte[] jsonResponse = Communicator.getMessageFromServer();
                    LeaveRoomResponse leaveRoom = Deserializer.DeserializerLeaveRoomResponse(jsonResponse);
                    Communicator.Logout();
                }
                else
                {
                    Communicator.Logout();
                }
                Communicator.CloseSocket();
            }
            catch (Exception er)
            {

            }
        }

    }
}
