﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using TriviaClient.RequestClasses;

namespace TriviaClient.classes
{
    static class Serializer
    {
        //make Signin request to message of bytes array
        public static byte[] SerializerRequest(SigninRequest signinRequest)
        {
            List<byte> fullMessageInBytes = new List<byte>();
            JObject json = new JObject();
            json.Add("username", signinRequest.username);
            json.Add("password", signinRequest.password);
            //add the code
            fullMessageInBytes.Add(BitConverter.GetBytes(Constants.LOGIN_CODE)[0]);
            //add the length of the json
            fullMessageInBytes.AddRange(BitConverter.GetBytes(json.ToString().Length));
            //add the json content
            fullMessageInBytes.AddRange(Encoding.ASCII.GetBytes(json.ToString()));

            return fullMessageInBytes.ToArray();
        }

        //make sign up request to message of bytes array
        public static byte[] SerializerRequest(SignupRequest signupRequest)
        {
            List<byte> fullMessageInBytes = new List<byte>();
            JObject json = new JObject();
            json.Add("username", signupRequest.username);
            json.Add("password", signupRequest.password);
            json.Add("email", signupRequest.email);
            json.Add("birthday", signupRequest.birthday);
            json.Add("address", signupRequest.address);
            json.Add("phoneNumber", signupRequest.phoneNumber);
            //add the code
            fullMessageInBytes.Add(BitConverter.GetBytes(Constants.SIGN_UP)[0]);
            //add the length of the json
            fullMessageInBytes.AddRange(BitConverter.GetBytes(json.ToString().Length));
            //add the json content
            fullMessageInBytes.AddRange(Encoding.ASCII.GetBytes(json.ToString()));

            return fullMessageInBytes.ToArray();
        }

        //make request that contains only code request to message of bytes array
        public static byte[] SerializerRequest(int code)
        {
            List<byte> fullMessageInBytes = new List<byte>();
            JObject json = new JObject();
            json.Add("code", code);
            //add the code
            fullMessageInBytes.Add(BitConverter.GetBytes(code)[0]);
            //add the length of the json
            fullMessageInBytes.AddRange(BitConverter.GetBytes(json.ToString().Length));
            //add the json content
            fullMessageInBytes.AddRange(Encoding.ASCII.GetBytes(json.ToString()));

            return fullMessageInBytes.ToArray();
        }

        //make create room request to message of bytes array
        public static byte[] SerializerRequest(CreateRoomRequest createRoomRequest)
        {
            List<byte> fullMessageInBytes = new List<byte>();
            JObject json = new JObject();
            json.Add("name", createRoomRequest.roomName);
            json.Add("maxUsers", createRoomRequest.maxUsers);
            json.Add("questionCount", createRoomRequest.questionCount);
            json.Add("answerTime", createRoomRequest.answerTimeout);

            //add the code
            fullMessageInBytes.Add(BitConverter.GetBytes(Constants.CREATE_ROOM_CODE)[0]);
            //add the length of the json
            fullMessageInBytes.AddRange(BitConverter.GetBytes(json.ToString().Length));
            //add the json content
            fullMessageInBytes.AddRange(Encoding.ASCII.GetBytes(json.ToString()));

            return fullMessageInBytes.ToArray();
        }

        //make get players request to message of bytes array
        public static byte[] SerializerRequest(GetPlayersInRoomRequest getPlayersInRoomRequest)
        {
            List<byte> fullMessageInBytes = new List<byte>();
            JObject json = new JObject();
            json.Add("roomId", getPlayersInRoomRequest.roomId);

            //add the code
            fullMessageInBytes.Add(BitConverter.GetBytes(Constants.GET_PLAYERS_CODE)[0]);
            //add the length of the json
            fullMessageInBytes.AddRange(BitConverter.GetBytes(json.ToString().Length));
            //add the json content
            fullMessageInBytes.AddRange(Encoding.ASCII.GetBytes(json.ToString()));

            return fullMessageInBytes.ToArray();
        }

        //make join room request to message of bytes array
        public static byte[] SerializerRequest(JoinRoomRequest joinRoomRequest)
        {
            List<byte> fullMessageInBytes = new List<byte>();
            JObject json = new JObject();
            json.Add("roomId", joinRoomRequest.roomId);

            //add the code
            fullMessageInBytes.Add(BitConverter.GetBytes(Constants.JOIN_ROOM_CODE)[0]);
            //add the length of the json
            fullMessageInBytes.AddRange(BitConverter.GetBytes(json.ToString().Length));
            //add the json content
            fullMessageInBytes.AddRange(Encoding.ASCII.GetBytes(json.ToString()));

            return fullMessageInBytes.ToArray();
        }

        //make submit answer request to message of bytes array
        public static byte[] SerializerRequest(SubmitAnswerRequest submitAnswerRequest)
        {
            List<byte> fullMessageInBytes = new List<byte>();
            JObject json = new JObject();
            json.Add("answerId", submitAnswerRequest.answerId);
            json.Add("answerTime", submitAnswerRequest.answerTime);

            //add the code
            fullMessageInBytes.Add(BitConverter.GetBytes(Constants.SUBMIT_ANSWER_CODE)[0]);
            //add the length of the json
            fullMessageInBytes.AddRange(BitConverter.GetBytes(json.ToString().Length));
            //add the json content
            fullMessageInBytes.AddRange(Encoding.ASCII.GetBytes(json.ToString()));

            return fullMessageInBytes.ToArray();
        }
    }
}
