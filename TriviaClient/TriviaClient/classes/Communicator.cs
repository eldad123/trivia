﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Windows;
using Newtonsoft.Json.Linq;
using TriviaClient.ResponseClasses;
using System.Runtime.CompilerServices;

namespace TriviaClient.classes
{
    static class Communicator
    {
        private static Socket sender; //the socket of the client

        public static void CloseSocket()
        {
            // Release the socket.  
            sender.Shutdown(SocketShutdown.Both);
            sender.Close();
        }

        //function will log out
        public static bool Logout()
        {
            try
            {
                Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.LOGOUT_CODE));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                LogoutResponse logoutResponse = Deserializer.DeserializerLogoutResponse(jsonResponse);
                return logoutResponse.status == Constants.SUCCESS_STATUS;
            }
            catch (Exception err) //if there was an error beacuse the server shut down
            {
                throw new Exception("There was a server error please try to connect again\n Error Details:" + err.Message);
            }
        }

        /*
        send message to the server
        input: message as array of bytes
        output: none
        */
        public static void sendMessageToServer(Byte[] data)
        {
            // Send the data through the socket.  
            int bytesSent = sender.Send(data);
        }

        /*
        get message from the server
        input: none
        output: array of bytes
        */
        public static byte[] getMessageFromServer()
        {
            byte[] code = new byte[Constants.CODE_LENGTH];
            byte[] jsonLength = new byte[Constants.INT_SIZE];
            byte[] jsonContent;
            // Receiv emessage code from the server.  
            sender.Receive(code, 0, code.Length, SocketFlags.None);
            // Receive json lengrh from the server.  
            sender.Receive(jsonLength, 0, Constants.INT_SIZE, SocketFlags.None);
            //set the length of the byte array in order to comsume
            jsonContent = new byte[BitConverter.ToInt32(jsonLength, 0)];
            //recieve the json content
            sender.Receive(jsonContent, 0, jsonContent.Length, SocketFlags.None);
            return jsonContent;
        }

        //create socket and be ready to start connect
        public static bool StartClient()
        {
            // Data buffer for incoming data.  
            byte[] bytes = new byte[1024];
            Socket sender;
            // Connect to a remote device.  
            try
            {
                // Establish the remote endpoint for the socket.  
                // This example uses port 11000 on the local computer.
                IPAddress ipAddress = IPAddress.Parse("127.0.0.1");
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 3000);

                // Create a TCP/IP  socket.  
                sender = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.  
                try
                {
                    sender.Connect(remoteEP);
                    sender.Receive(bytes, 0, Constants.HELLO_MSG_LENGTH, SocketFlags.None);
                    
                }
                catch (ArgumentNullException ane)
                {
                    return false;
                   
                }
                catch (SocketException se)
                {
                    return false;
                }
                catch (Exception e)
                {
                    return false;
                }

            }
            catch (Exception e)
            {
                return false;
            }
            Communicator.sender = sender;
            return true;
        }
    }

}
