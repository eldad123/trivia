﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using TriviaClient.ResponseClasses;
using TriviaClient.DataClasses;

namespace TriviaClient.classes
{
    class Deserializer
    {
        //make server response of bytes array to log out response (class)
        public static LogoutResponse DeserializerLogoutResponse(byte[] serverResponse)
        {
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new LogoutResponse(json.Value<string>("message"));
            }
            return new LogoutResponse(json.Value<int>("status"));
        }

        //make server response of bytes array to leave room response (class)
        public static LeaveRoomResponse DeserializerLeaveRoomResponse(byte[] serverResponse)
        {
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new LeaveRoomResponse(json.Value<string>("message"));
            }
            return new LeaveRoomResponse(json.Value<int>("status"));
        }

        //make server response of bytes array to sign in response (class)
        public static SigninResponse DeserializerSigninResponse(byte[] serverResponse)
        {
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new SigninResponse(json.Value<string>("message"));
            }
            return new SigninResponse(json.Value<int>("status"));
        }

        /*
        function will make a sign up response that contains the error that occurred
        input: json message from the server
        output: sign up response that contains the error that occurred
        */
        private static SignupResponse FillTheRightError(JObject json)
        {
            string error = json.Value<string>("error").Substring(json.Value<string>("error").IndexOf(":") + 2);
            string typeOfError = json.Value<string>("error").Substring(0, json.Value<string>("error").IndexOf(":"));
            SignupResponse signup = new SignupResponse(Constants.FAILD_STATUS);
            switch (typeOfError)
            {
                case "username":
                    signup.usernameError = error;
                    break;
                case "password":
                    signup.passwordError = error;
                    break;
                case "email":
                    signup.emailError = error;
                    break;
                case "birthday":
                    signup.birthdayError = error;
                    break;
                case "address":
                    signup.addressError = error;
                    break;
                case "phoneNumber":
                    signup.phoneError = error;
                    break;

            }
            return signup;
        }

        //make server response of bytes array to sign up response (class)
        public static SignupResponse DeserializerSignupResponse(byte[] serverResponse)
        {
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new SignupResponse(json.Value<string>("message"));
            }
            if (json.ContainsKey("error"))
            {
                return FillTheRightError(json);
            }
            return new SignupResponse(json.Value<int>("status"));
        }

        //make server response of bytes array to sbest score response (class)
        public static BestScoresResponse DeserializerBestScoreResponse(byte[] serverResponse)
        {
            BestScoresResponse bestScores = new BestScoresResponse();
            List<User> top = new List<User>();
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new BestScoresResponse(json.Value<string>("message"));
            }
            bestScores.top5 = json.Value<JArray>("BestScores").ToObject<List<User>>();
            bestScores.status = json.Value<int>("status");
            return bestScores;
        }

        //make server response of bytes array statistics in response (class)
        public static StatisticsResponse DeserializerStatisticsResponse(byte[] serverResponse)
        {
            StatisticsResponse statistics = new StatisticsResponse();
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new StatisticsResponse(json.Value<string>("message"));
            }
            statistics.status = json.Value<int>("status");
            json = json.Value<JObject>("UserStatistics");
            statistics.gamesNumber = json.Value<string>("games number");
            statistics.correctAnswers = json.Value<string>("currect answers");
            statistics.averageTime = json.Value<string>("average time");
            statistics.totalAnswers = json.Value<string>("total answers");
            return statistics;
        }

        //make server response of bytes array to create or join room (that has same feilds) in response (class)
        public static CreateAndJoinRoomResponse DeserializerCreateAndJoinRoomResponse(byte[] serverResponse)
        {
            CreateAndJoinRoomResponse createOrJoin = new CreateAndJoinRoomResponse();
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new CreateAndJoinRoomResponse(json.Value<string>("message"));
            }
            createOrJoin.status = json.Value<int>("status");
            return createOrJoin;
        }

        //make server response of bytes array to get state response (class)
        public static GetRoomStateResponse DeserializerGetRoomState(byte[] serverResponse)
        {
            GetRoomStateResponse getState = new GetRoomStateResponse();
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new GetRoomStateResponse(json.Value<string>("message"));
            }
            getState.status = json.Value<int>("status");
            if (getState.status == Constants.SUCCESS_STATUS)
            {
                getState.hasGameBegun = json.Value<bool>("hasGameBegun");
                getState.players = json.Value<JArray>("players").ToObject<List<string>>();
                getState.questionCount = json.Value<int>("questionCount");
                getState.answerTimeout = json.Value<int>("answerTimeOut");
            }
            else
            {
                getState.err = json.Value<string>("error");
            }
            return getState;
        }

        //make server response of bytes array to get rooms response (class)
        public static GetRoomsResponse DeserializerGetRoomsResponse(byte[] serverResponse)
        {
            GetRoomsResponse getRooms = new GetRoomsResponse();
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new GetRoomsResponse(json.Value<string>("message"));
            }
            getRooms.roomDataLst = json.Value<JArray>("Rooms").ToObject<List<RoomData>>();
            return getRooms;
        }

        //make server response of bytes array to get players in room response (class)
        public static GetPlayersInRoomResponse DeserializerGetPlayersInRoomResponse(byte[] serverResponse)
        {
            GetPlayersInRoomResponse getPlayers = new GetPlayersInRoomResponse();
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new GetPlayersInRoomResponse(json.Value<string>("message"));
            }
            getPlayers.players = json.Value<string>("Players");
            return getPlayers;
        }

        //make server response of bytes array to submit answer response (class)
        public static SubmitAnswerResponse DeserializerSubmitAnswerResponse(byte[] serverResponse)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<SubmitAnswerResponse>(Encoding.UTF8.GetString(serverResponse));
            //JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            //return new SubmitAnswerResponse(json.Value<int>("status"), json.Value<int>("correctAnswerId"));
        }

        //make server response of bytes array to get question response (class)
        public static GetQuestionResponse DeserializerGetQuestionResponse(byte[] serverResponse, Random rnd)
        {
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            Dictionary<int, string> answers = new Dictionary<int, string>();
            if (json.ContainsKey("message"))
            {
                return new GetQuestionResponse(json.Value<string>("message"));
            }
            foreach (JObject answer in json.Value<JArray>("answers").ToObject<List<JObject>>())
            {
                answers.Add(answer.Value<int>("number"), answer.Value<string>("answer"));
            }
            return new GetQuestionResponse(json.Value<int>("status"), json.Value<string>("question"), answers, rnd);
        }

        //make server response of bytes array to leave game response (class)
        public static LeaveGameResponse DeserializerLeaveGameResponse(byte[] serverResponse)
        {
            return Newtonsoft.Json.JsonConvert.DeserializeObject<LeaveGameResponse>(Encoding.UTF8.GetString(serverResponse));
        }

        //make server response of bytes array to get game results response (class)
        public static GetGameResultsResponse DeserializerGetGameResultsResponse(byte[] serverResponse)
        {
            GetGameResultsResponse gameResults = new GetGameResultsResponse();
            JObject json = JObject.Parse(Encoding.UTF8.GetString(serverResponse));
            if (json.ContainsKey("message"))
            {
                return new GetGameResultsResponse(json.Value<string>("message"));
            }
            gameResults.status = json.Value<int>("status");
            if (gameResults.status == Constants.SUCCESS_STATUS)
            {
                gameResults.results = json.Value<JArray>("results").ToObject<List<PlayerResults>>();
            }
            return gameResults;
        }

    }
}
