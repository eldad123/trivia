﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.classes
{
    static class Constants
    {
        public const int ACTIVE_ERROR_CODE = -1;
        public const int NO_PLACE_ERROR_CODE = -2;

        public const int SUCCESS_STATUS = 1;
        public const int ACTIVE = 1;
        public const int FAILD_STATUS = 0;

        public const int HELLO_MSG_LENGTH = 5;
        public const int INT_SIZE = 4;
        public const int CODE_LENGTH = 1;

        public const int SIGN_UP = 0;
        public const int LOGIN_CODE = 1;
        public const int INDEX_OF_CORRECT_ANSWERS = 0;
        public const int ERROR_CODE = 2;
        public const int LOGOUT_CODE = 3;

        public const int CREATE_ROOM_CODE = 4;
        public const int JOIN_ROOM_CODE = 5;
        public const int GET_ROOMS_CODE = 6;
        public const int GET_PLAYERS_CODE = 7;
        public const int GET_STATS_CODE = 8;
        public const int CLOSE_ROOM_CODE = 9;
        public const int GET_ROOM_STATE_CODE = 11;
        public const int LEAVE_ROOM_CODE = 12;

        public const int START_GAME_CODE = 10;
        public const int GET_RESULTS_CODE = 13;
        public const int SUBMIT_ANSWER_CODE = 14;
        public const int GET_QUESTION_CODE = 15;
        public const int LEAVE_GAME_CODE = 16;

        public const int MAX_QUESTIONS = 50;

        public const string ADMIN_LEFT_ERROR = "The admin has just left the room";
    }
}
