﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.classes
{
    class User
    {
        public string username { get; set; }
        public string score { get; set; }

        [JsonConstructor]
        public User(string username, string score)
        {
            this.username = username;
            this.score = score;
        }
        
        public User(string username)
        {
            this.username = username;
        }
        
        public string GetScore()
        {
            return score;
        }
        public string GetName()
        {
            return this.username;
        }
        public void SetName(string username)
        {
            this.username = username;
        }
        public void SetScore(string score)
        {
            this.score = score;
        }
    }
}
