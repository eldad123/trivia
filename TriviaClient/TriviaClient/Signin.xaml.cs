﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.Diagnostics;
using TriviaClient.classes;
using Newtonsoft.Json.Linq;
using TriviaClient.ResponseClasses;
using TriviaClient.RequestClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for Signin.xaml
    /// </summary>
    public partial class Signin : Page
    {
        public Signin()
        {
            InitializeComponent();
        }

        //This function detect when the HyperLink is pressed and navigate to signun page when he does
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            //navigate no signin page
            this.NavigationService.Navigate(new Signup());
        }

        //this fronction do the the sginin action 
        //input:none
        //output:none
        private void signinAction()
        {
            try
            {
                checkboxError.Text = "";
                serverError.Text = "";
                //check if the "I am not a robot" checkbox is checked
                if (notARobot.IsChecked ?? true)//if it does
                {
                    //create the sign request structer with data
                    SigninRequest signinRequest = new SigninRequest(username.Text, password.Password);
                    //send the message to the client
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(signinRequest));
                    //get the message from the client
                    byte[] jsonResponse = Communicator.getMessageFromServer();
                    //Deserializer the message from the server
                    SigninResponse signinResponse = Deserializer.DeserializerSigninResponse(jsonResponse);
                    //if the message is not cvalid at this point
                    if(signinResponse.errMessage == "The message in not valid at this point")
                    {
                        MessageBox.Show("This option is invalid at this point", "Error");
                    }
                    else if (signinResponse.status == Constants.SUCCESS_STATUS)// if the user signin succesfully
                    {
                        this.NavigationService.Navigate(new MenuPage(username.Text));
                    }
                    else//if there was an error in the signin
                    {
                        serverError.Text = "The details are incorrect";
                    }
                }
                else
                {
                    checkboxError.Text = "you must check this checkbox";
                }
            }
            catch (Exception err)//if there was an error beacuse the server is shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }
        }

        //This fuction is activate when the enter key is pressed
        private void OnEnterKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                signinAction();//do the signin
            }
        }
        //this function check when the signin button is pressed 
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            signinAction();//do the signin
        }
    }
}
