﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for MenuPage.xaml
    /// </summary>
    public partial class MenuPage : Page
    {
        public string username;
        public MenuPage(string username)
        {
            InitializeComponent();
            this.username = username;
        }

        /*
        pressed on menu's options - function will go to the wanted page
        */

        private void join_room_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new JoinRoom(username));
        }

        private void create_room_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new CreateRoom(username));
        }

        private void logout_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Communicator.Logout())
                {
                    this.NavigationService.Navigate(new Signin());
                }
            }
            catch (Exception err)//if the server shutdown
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }
        }

        private void statistics_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new StatisticsMenu(username));
        }
    }
}
