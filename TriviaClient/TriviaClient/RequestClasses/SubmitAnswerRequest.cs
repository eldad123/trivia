﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.RequestClasses
{
    class SubmitAnswerRequest
    {
        public int answerId;
        public int answerTime;

        public SubmitAnswerRequest(int answerId, int answerTime)
        {
            this.answerId = answerId;
            this.answerTime = answerTime;
        }
    }
}
