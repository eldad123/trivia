﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.RequestClasses
{
    class JoinRoomRequest
    {
        public int roomId;

        public JoinRoomRequest(int roomId)
        {
            this.roomId = roomId;
        }
    }
}
