﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.RequestClasses
{
    class GetPlayersInRoomRequest
    {
        public int roomId;

        public GetPlayersInRoomRequest(int roomId)
        {
            this.roomId = roomId;
        }
    }
}
