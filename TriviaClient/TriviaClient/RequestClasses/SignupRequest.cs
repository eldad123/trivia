﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.RequestClasses
{
    class SignupRequest
    {

        public string username;
        public string password;
        public string email;
        public string birthday;
        public string address;
        public string phoneNumber;

        public SignupRequest(string username, string password, string email, string birthday, string address, string phoneNumber)
        {
            this.username = username;
            this.password = password;
            this.email = email;
            this.birthday = birthday;
            this.address = address;
            this.phoneNumber = phoneNumber;
        }

    }
}
