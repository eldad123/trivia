﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;
using TriviaClient.ResponseClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for GameResults.xaml
    /// </summary>
    public partial class GameResults : Page
    {
        private string username;
        private BackgroundWorker background_worker = new BackgroundWorker();

        public GameResults(string username)
        {
            InitializeComponent();
            this.username = username;

            //the thread that work in the background
            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;

            background_worker.DoWork += background_worker_DoWork;
            background_worker.ProgressChanged += background_worker_ProgressChanged;
            background_worker.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync(1000);
        }

        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }

        void background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            //get result ans print it
            if (((GetGameResultsResponse)e.UserState).status == Constants.SUCCESS_STATUS) //if scceeed
            {
                this.results.Text = "The results are: ";
                this.dataGrid.ItemsSource = ((GetGameResultsResponse)e.UserState).results;
                this.Winner.Text = "The winner of this game is: " + this.GetWinner(((GetGameResultsResponse)e.UserState).results);
                this.dataGrid.Visibility = Visibility.Visible;
                background_worker.CancelAsync();
            }
        }

        void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                if (background_worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;
                }
                try
                {
                    //try to get results from the server
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.GET_RESULTS_CODE));
                    byte[] jsonResponse = Communicator.getMessageFromServer();
                    GetGameResultsResponse resultsResponse = Deserializer.DeserializerGetGameResultsResponse(jsonResponse);
                    background_worker.ReportProgress(1, resultsResponse);
                }
                catch (Exception err) //if there was an error beacuse the server shut down
                {
                    MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                    background_worker.CancelAsync();
                }
                Thread.Sleep(2000);
            }
        }

        //pressed back
        private void back_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //try to leave the game
                Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.LEAVE_GAME_CODE));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                LeaveGameResponse leaveResponse = Deserializer.DeserializerLeaveGameResponse(jsonResponse);
                background_worker.CancelAsync();
                this.NavigationService.Navigate(new MenuPage(this.username));
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                background_worker.CancelAsync();
            }
        }

        /*
        function will find the winner by the game results
        input: results
        output: the name of the winner
        */
        private string GetWinner(List<PlayerResults> results)
        {
            PlayerResults winnerPlayer = results[0];
            foreach (PlayerResults playerResults in results)
            {
                if (winnerPlayer.correctAnswerCount ==  playerResults.correctAnswerCount && winnerPlayer.averageAnswerTime > playerResults.averageAnswerTime)
                {
                    winnerPlayer = playerResults;
                }
                else if (playerResults.GetScore() > winnerPlayer.GetScore())
                {
                    winnerPlayer = playerResults;
                }
            }
            return winnerPlayer.username;
        }

    }
}
