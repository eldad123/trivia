﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for StatisticsMenu.xaml
    /// </summary>
    public partial class StatisticsMenu : Page
    {
        public string username;
        public StatisticsMenu(string username)
        {
            InitializeComponent();
            this.username = username;
        }

        private void my_statistics_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Statistics(this.username));
        }

        private void best_scores_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new BestScores(this.username));
        }

        private void back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MenuPage(this.username));
        }
    }
}
