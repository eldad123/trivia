﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using TriviaClient.classes;
using TriviaClient.ResponseClasses;
using TriviaClient.RequestClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for Signup.xaml
    /// </summary>
    public partial class Signup : Page
    {
        public Signup()
        {
            InitializeComponent();
        }
        
        //This function activate when the hyperlink is pressed and navigate to the Signin page
        private void Hyperlink_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Signin());
        }

        //this function do the signup action
        private void signupAction()
        {
            try
            {
                checkboxError.Text = "";
                usernameError.Text = "";
                passwordError.Text = "";
                serverError.Text = "";
                birthdayError.Text = "";
                addressError.Text = "";
                phoneError.Text = "";
                emailError.Text = "";
                //check if the "I am not a robot" chackbox is pressed
                if (notARobot.IsChecked ?? true)//if it does
                {
                    if (birthDate.SelectedDate == null)
                    {
                        birthdayError.Text = "you must choose a date";
                    }
                    else
                    {
                        //send the signup message to the server and get response from him
                        SignupRequest signupRequest = new SignupRequest(username.Text, password.Password, email.Text, birthDate.SelectedDate.Value.ToString("dd/MM/yyyy"), address.Text, phoneNumber.Text);
                        Communicator.sendMessageToServer(Serializer.SerializerRequest(signupRequest));
                        byte[] jsonResponse = Communicator.getMessageFromServer();
                        SignupResponse signupResponse = Deserializer.DeserializerSignupResponse(jsonResponse);
                        //if the message is not valid at this point
                        if (signupResponse.errMessage == "The message in not valid at this point")
                        {
                            MessageBox.Show("This option is invalid at this point", "Error");
                        }
                        else if (signupResponse.status == Constants.SUCCESS_STATUS)//if the signup worked
                        {
                            this.NavigationService.Navigate(new MenuPage(username.Text));
                        }
                        else//if the signup failed
                        {
                            //set the errors to the the rignt places
                            passwordError.Text = signupResponse.passwordError;
                            birthdayError.Text = signupResponse.birthdayError;
                            addressError.Text = signupResponse.addressError;
                            phoneError.Text = signupResponse.phoneError;
                            emailError.Text = signupResponse.emailError;
                            usernameError.Text = signupResponse.usernameError;
                        }
                    }
                }
                else
                {
                    checkboxError.Text = "you must check this checkbox";
                }
                    
            }
            catch (Exception err)//if there was an error beacuse the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }
        }

        //This function activate when the user press enter and send signup request
        private void OnEnterKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                signupAction();
            }
        }

        //This function activate when the user press the signup button and send signup request
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            signupAction();
        }
    }
}
