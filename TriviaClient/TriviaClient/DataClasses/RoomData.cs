﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.DataClasses
{
    class RoomData
    {
        public int id { get; set; }
        public string name { get; set; }
        public int maxPlayers { get; set; }
        public int timePerQuestion { get; set; }
        public int questionCount { get; set; }
        public int isActive { get; set; }
        public string players { get; set; }

        public RoomData(int id, int isActive, int maxPlayers, string name, int questionsCount, int timePerQuestion)
        {
            this.id = id;
            this.name = name;
            this.maxPlayers = maxPlayers;
            this.timePerQuestion = timePerQuestion;
            this.questionCount = questionsCount;
            this.isActive = isActive;
        }

        
    }
}
