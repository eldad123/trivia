﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;
using TriviaClient.ResponseClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for BestScores.xaml
    /// </summary>
    public partial class BestScores : Page
    {
        public string username;
        public BestScores(string username)
        {
            InitializeComponent();
            this.username = username;
            try
            {
                //try to get the best score
                Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.GET_STATS_CODE));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                BestScoresResponse bestScoresResponse = Deserializer.DeserializerBestScoreResponse(jsonResponse);
                if (bestScoresResponse.errMessage == "The message in not valid at this point") //if there is an error of relevant
                {
                    MessageBox.Show("This option is invalid at this point", "Error");
                    this.NavigationService.Navigate(new MenuPage(username));
                }
                else if (bestScoresResponse.status == Constants.SUCCESS_STATUS) //if secceeded
                {
                    dataGrid.ItemsSource = bestScoresResponse.top5; //show the top 5
                }
                else
                {
                    throw new Exception();
                }

            }
            catch (Exception err) //if there was an error beacuse the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }
        }

        //pressed back
        private void back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new StatisticsMenu(this.username));
        }
    }
}
