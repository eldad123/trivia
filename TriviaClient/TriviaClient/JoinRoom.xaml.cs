﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;
using TriviaClient.DataClasses;
using TriviaClient.RequestClasses;
using TriviaClient.ResponseClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for JoinRoom.xaml
    /// </summary>
    public partial class JoinRoom : Page
    {
        private RoomData selectedRow = null;
        private string username;
        private int questionsCount;
        private int timePerQuestions;
        private int maxPlayersNum;

        public JoinRoom(string username)
        {
            InitializeComponent();
            this.username = username;
            try
            {
                GetRoomsResponse getRooms = GetRoomsData(); //get all the rooms 

                //get the roons that available
                GetRoomsResponse getRoomsThatTheUserCanJoin = new GetRoomsResponse();
                for (int i = 0; i < getRooms.roomDataLst.Count; i++)
                {
                    if (getRooms.roomDataLst[i].isActive != Constants.ACTIVE &&
                        this.CountCharInString(',', getRooms.roomDataLst[i].players) + 1 < getRooms.roomDataLst[i].maxPlayers)
                    {
                        getRoomsThatTheUserCanJoin.roomDataLst.Add(getRooms.roomDataLst[i]);
                    }
                }
                dataGrid.ItemsSource = getRoomsThatTheUserCanJoin.roomDataLst;
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }

        }

        //get the eooms that the user can join to
        private GetRoomsResponse GetRoomsData()
        {
            //get all the rooms
            Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.GET_ROOMS_CODE));
            byte[] jsonResponseForRooms = Communicator.getMessageFromServer();
            GetRoomsResponse getRooms = Deserializer.DeserializerGetRoomsResponse(jsonResponseForRooms);

            for (int i = 0; i < getRooms.roomDataLst.Count; i++)
            {
                GetPlayersInRoomRequest playersRequest = new GetPlayersInRoomRequest(getRooms.roomDataLst[i].id);
                Communicator.sendMessageToServer(Serializer.SerializerRequest(playersRequest));
                byte[] jsonResponseForPlayers = Communicator.getMessageFromServer();
                GetPlayersInRoomResponse getPlayers = Deserializer.DeserializerGetPlayersInRoomResponse(jsonResponseForPlayers);
                getRooms.roomDataLst[i].players = getPlayers.players;
            }

            return getRooms;
        }

        internal RoomData SelectedRow { get => selectedRow; set => selectedRow = value; }

        //function will get the room that was selected
        private void ItemsView_OnSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.RemovedItems.Count == 0)
            {
                RoomData item = (RoomData)(e.AddedItems[0]);
                this.selectedRow = item;
                this.questionsCount = item.questionCount;
                this.timePerQuestions = item.timePerQuestion;
                this.maxPlayersNum = item.maxPlayers;
            }
            else
            {
                this.selectedRow = null;
            }
        }

        private void Join_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (this.selectedRow == null)
                {
                    this.Error.Text = "You must choose a room before you click join";
                }
                else
                {
                    this.Error.Text = "";
                    int roomId = this.selectedRow.id;
                    bool roomFound = false;
                    GetRoomsResponse getRooms = GetRoomsData(); //get the rooms

                    for (int i = 0; i < getRooms.roomDataLst.Count; i++)
                    {
                        if (getRooms.roomDataLst[i].id == roomId)
                        {
                            roomFound = true;

                        }
                    }
                    if (!roomFound) //if the room has not found
                    {
                        this.Error.Text = "The room has closed";
                    }
                    else
                    {
                        //if the room found, join it
                        JoinRoomRequest joinRoomRequest = new JoinRoomRequest(roomId);
                        Communicator.sendMessageToServer(Serializer.SerializerRequest(joinRoomRequest));
                        byte[] jsonResponse = Communicator.getMessageFromServer();
                        CreateAndJoinRoomResponse joinRoom = Deserializer.DeserializerCreateAndJoinRoomResponse(jsonResponse);
                        if (joinRoom.status == Constants.SUCCESS_STATUS) //if secceeded
                        {
                            this.NavigationService.Navigate(new WaitForGameToStart(this.selectedRow.name, this.username, this.questionsCount, this.timePerQuestions));
                        }
                        else if(joinRoom.status == Constants.ACTIVE_ERROR_CODE) //if the room if full
                        {
                            this.Error.Text = "A game has started in the room";
                        }
                        else if (joinRoom.status == Constants.NO_PLACE_ERROR_CODE) //if the room has closed
                        {
                            this.Error.Text = "The room is full of players";
                        }
                    }
                }
            }
            catch (Exception err) //if there was an error beacuse the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }

        }

        //refresh the list of rooms
        private void Refresh_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                this.Error.Text = "";
                this.selectedRow = null;
                GetRoomsResponse getRooms = GetRoomsData(); //get all the rooms
                //get the roons that available
                GetRoomsResponse getRoomsThatTheUserCanJoin = new GetRoomsResponse();
                for(int i = 0; i < getRooms.roomDataLst.Count; i++)
                {
                    if(getRooms.roomDataLst[i].isActive != Constants.ACTIVE &&
                        this.CountCharInString(',', getRooms.roomDataLst[i].players) + 1 < getRooms.roomDataLst[i].maxPlayers)
                    {
                        getRoomsThatTheUserCanJoin.roomDataLst.Add(getRooms.roomDataLst[i]);
                    }
                }
                dataGrid.ItemsSource = getRoomsThatTheUserCanJoin.roomDataLst;
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }
        }

        //pressed back
        private void back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MenuPage(this.username));
        }

        private void dataGrid_OnUnSelected(object sender, MouseButtonEventArgs e)
        {
            ((DataGrid)sender).SelectedItem = null;
            this.selectedRow = null;
        }

        /*
        function will count how many times the char in string
        input: char and string
        output: how many times the char in string
        */
        private int CountCharInString(char c, string s)
        {
            int count = 0;
            foreach (char ch in s)
            {
                if (c == ch)
                {
                    count++;
                }
            }
            return count;
        }
    }
}
