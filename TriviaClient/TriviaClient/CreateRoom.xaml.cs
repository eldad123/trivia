﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;
using TriviaClient.ResponseClasses;
using Newtonsoft.Json.Linq;
using TriviaClient.RequestClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for CreateRoom.xaml
    /// </summary>
    public partial class CreateRoom : Page
    {
        public string username;
        public CreateRoom(string username)
        {
            InitializeComponent();
            this.username = username;
        }

        /*
        function will check if a string contains only digits
        input: string
        output: if a string contains only digits
        */
        private bool IsAllDigits(string s)
        {
            if(s[0] == '-')
            {
                s = s.Substring(1);
            }
            foreach (char c in s)
            {
                if (!char.IsDigit(c))
                {
                    return false;
                }
            }
            return true;
        }

        //check which error ocurred and shot it
        private void CheckWhichErrorOccured()
        {
            if (roomName.Text == "")
                roomNameError.Text = "You must enter this field";

            if (numOfPlayers.Text == "")
                numOfPlayersError.Text = "You must enter this field";
            else if (!(IsAllDigits(numOfPlayers.Text)))
                numOfPlayersError.Text = "You must enter only numbers";
            else if (int.Parse(numOfPlayers.Text) <= 0)
                numOfPlayersError.Text = "Number of players must be bigger than 0";

            if (numOfQuestions.Text == "")
                numOfQuestionsError.Text = "You must enter this field";
            else if (!(IsAllDigits(numOfQuestions.Text)))
                numOfQuestionsError.Text = "You must enter only numbers";
            else if (int.Parse(numOfQuestions.Text) <= 0 || int.Parse(numOfQuestions.Text) > Constants.MAX_QUESTIONS)
                numOfQuestionsError.Text = "Number of questions must be bigger than 0 and lower than 51";

            if (timeForQuestion.Text == "")
                timeForQuestionError.Text = "You must enter this field";
            else if (!(IsAllDigits(timeForQuestion.Text)))
                timeForQuestionError.Text = "You must enter only numbers";
            else if (int.Parse(timeForQuestion.Text) <= 0)
                timeForQuestionError.Text = "Time for questions must be bigger than 0";

        }

        //pressed create room
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            CreateRoomFunction();
        }

        //pressed enter - is like pressed create room
        private void OnEnterKey(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                CreateRoomFunction();
            }
        }

        //pressed back
        private void back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new MenuPage(username));
        }

        //try to create room
        private void CreateRoomFunction()
        {
            try
            {
                roomNameError.Text = "";
                numOfPlayersError.Text = "";
                numOfQuestionsError.Text = "";
                timeForQuestionError.Text = "";
                //if all the parameters are valid
                if (roomName.Text != "" && numOfPlayers.Text != "" && numOfQuestions.Text != "" && timeForQuestion.Text != ""
                    && (IsAllDigits(numOfPlayers.Text)) && (IsAllDigits(numOfQuestions.Text)) && (IsAllDigits(timeForQuestion.Text))
                    && int.Parse(numOfPlayers.Text) > 0 && int.Parse(numOfQuestions.Text) > 0 && int.Parse(timeForQuestion.Text) > 0 
                    && int.Parse(numOfQuestions.Text) <= Constants.MAX_QUESTIONS)
                {
                    //try to create room
                    CreateRoomRequest createRoomRequest = new CreateRoomRequest(roomName.Text, int.Parse(numOfPlayers.Text), int.Parse(numOfQuestions.Text), int.Parse(timeForQuestion.Text));
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(createRoomRequest));
                    byte[] jsonResponse = Communicator.getMessageFromServer();
                    CreateAndJoinRoomResponse createRoomResponse = Deserializer.DeserializerCreateAndJoinRoomResponse(jsonResponse);
                    if (createRoomResponse.status == Constants.SUCCESS_STATUS) //if secceeded
                    {
                        this.NavigationService.Navigate(new StartGame(roomName.Text, this.username, int.Parse(numOfQuestions.Text), int.Parse(timeForQuestion.Text))); //go to start game page
                    }
                }
                else
                {
                    CheckWhichErrorOccured();
                }

            }
            catch (Exception err) //if there was an error beacuse the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }
        }
    }
}
