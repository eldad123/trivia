﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;
using TriviaClient.ResponseClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for Statistics.xaml
    /// </summary>
    public partial class Statistics : Page
    {
        public string username;
        public Statistics(string username)
        {
            InitializeComponent();
            this.username = username;

            try
            {
                //send get statistics request to the server and get response
                Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.GET_STATS_CODE));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                StatisticsResponse statisticsResponse = Deserializer.DeserializerStatisticsResponse(jsonResponse);
                //if the message is invalid at this point
                if (statisticsResponse.errMessage == "The message in not valid at this point")
                {
                    MessageBox.Show("This option is invalid at this point", "Error");
                    this.NavigationService.Navigate(new MenuPage(username));
                }
                else if (statisticsResponse.status == Constants.SUCCESS_STATUS)// if the statistics request succeed
                {
                    //put all the statistics in their text blocks
                    correct_answers.Text += statisticsResponse.correctAnswers;
                    total_answers.Text += statisticsResponse.totalAnswers;
                    games_number.Text += statisticsResponse.gamesNumber;
                    averge_time.Text += statisticsResponse.averageTime;
                }
                else//if faild to get statistics
                {
                    correct_answers.Text += "Error, please try again";
                    total_answers.Text += "Error, please try again";
                    games_number.Text += "Error, please try again";
                    averge_time.Text += "Error, please try again";
                }
            }
            catch(Exception er)//if the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + er.Message, "Error");
            }
        }

        //This function activate when back button is pressed. It navigate to the statistics menu
        private void back_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new StatisticsMenu(this.username));
        }

    }
}
