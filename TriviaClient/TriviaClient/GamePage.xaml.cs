﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;
using TriviaClient.RequestClasses;
using TriviaClient.ResponseClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for GamePage.xaml
    /// </summary>
    public partial class GamePage : Page
    {
        public string username;
        public string roomName;

        private int questionsCount;
        private int timePerQuestions;
        private int correctAnswersCount = 0;
        private int currentQuestionNum = 0;
        private int currTime = 0;

        private bool answered = false;
        private bool userExited = false;

        private int Answer1Key = 0;
        private int Answer2Key = 0;
        private int Answer3Key = 0;
        private int Answer4Key = 0;
        private Random random = new Random();

        private BackgroundWorker background_worker = new BackgroundWorker();

        public GamePage(string roomName, string username, int questionsCount, int timePerQuestions)
        {
            InitializeComponent();
            this.roomName = roomName;
            this.roomNameBlock.Text = roomName;
            this.username = username;
            this.timePerQuestions = timePerQuestions;
            this.questionsCount = questionsCount;

            //the thread in the background
            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;

            background_worker.DoWork += background_worker_DoWork;
            background_worker.ProgressChanged += background_worker_ProgressChanged;
            background_worker.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync(questionsCount);
        }

        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            //the thread stopped
            if(!e.Cancelled && !this.userExited)
            {
                this.NavigationService.Navigate(new GameResults(this.username));
            }
        }

        void background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            if (e.UserState is int && (int)e.UserState == -1)
            {
                AnswerButtonAction((int)e.UserState);
                this.answered = false;
            }
            else
            {
                //get the question and answers
                GetQuestionResponse question = (GetQuestionResponse)e.UserState;

                ResetButtonsColors();

                this.Answer1.Content = question.GetAnswers().ElementAt(0).Value;
                this.Answer1Key = question.GetAnswers().ElementAt(0).Key;

                this.Answer2.Content = question.GetAnswers().ElementAt(1).Value;
                this.Answer2Key = question.GetAnswers().ElementAt(1).Key;

                this.Answer3.Content = question.GetAnswers().ElementAt(2).Value;
                this.Answer3Key = question.GetAnswers().ElementAt(2).Key;

                this.Answer4.Content = question.GetAnswers().ElementAt(3).Value;
                this.Answer4Key = question.GetAnswers().ElementAt(3).Key;
                this.question.Text = question.question;
                this.questions.Text = "Question: " + this.currentQuestionNum.ToString() + " / " + this.questionsCount.ToString();
                this.Timer.Text = "Time left: " + currTime.ToString() + " / " + this.timePerQuestions.ToString();
            }
            EnableAllButtons();
        }

        //function will do the work of get data of game
        void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                bool IsCanceled = false;
                for (int i = 1; i <= this.questionsCount && !IsCanceled; i++)
                {
                    this.currentQuestionNum = i;
                    this.currTime = this.timePerQuestions;
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.GET_QUESTION_CODE));
                    byte[] jsonResponse = Communicator.getMessageFromServer();
                    GetQuestionResponse question = Deserializer.DeserializerGetQuestionResponse(jsonResponse, this.random);

                    for (int j = 1; j <= this.timePerQuestions; j++)
                    {
                        if (background_worker.CancellationPending)
                        {
                            e.Cancel = true;
                            IsCanceled = true;
                            break;
                        }
                        background_worker.ReportProgress(j, question);
                        Thread.Sleep(1000);
                        if (this.answered)
                        {
                            Thread.Sleep(500);
                            break;
                        }
                        this.currTime--;


                    }
                    if (!answered)
                    {
                        background_worker.ReportProgress(i, -1);
                        Thread.Sleep(500);
                    }
                    this.answered = false;
                }
            }
            catch (Exception err) //if there was an error beacuse the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                e.Cancel = true;
            }
        }

        private void Answer1_Click(object sender, RoutedEventArgs e)
        {
            AnswerButtonAction(this.Answer1Key);
        }

        private void Answer2_Click(object sender, RoutedEventArgs e)
        {
            AnswerButtonAction(this.Answer2Key);
        }

        private void Answer3_Click(object sender, RoutedEventArgs e)
        {
            AnswerButtonAction(this.Answer3Key);
        }

        private void Answer4_Click(object sender, RoutedEventArgs e)
        {
            AnswerButtonAction(this.Answer4Key);
        }

        //pressed exit game
        private void Exit_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                //try to leave the game
                this.userExited = true;
                background_worker.CancelAsync();
                this.currTime = 0;
                AnswerButtonAction(-1);
                Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.LEAVE_GAME_CODE));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                LeaveGameResponse leaveGameResponse = Deserializer.DeserializerLeaveGameResponse(jsonResponse);
                this.NavigationService.Navigate(new MenuPage(this.username));
            }
            catch (Exception err)
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                background_worker.CancelAsync();
            }
        }

        private void AnswerButtonAction(int answerId)
        {
            try
            {
                //submit the answer
                if(this.currTime == this.timePerQuestions)
                {
                    this.currTime--;
                }
                Communicator.sendMessageToServer(Serializer.SerializerRequest(new SubmitAnswerRequest(answerId, (this.timePerQuestions - this.currTime))));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                SubmitAnswerResponse submit = Deserializer.DeserializerSubmitAnswerResponse(jsonResponse);
                if (submit.correctAnswerId == answerId) //if the answer is correct
                {
                    this.correctAnswersCount++; //add 1 tp the user correct answers
                    this.CorrectAnswers.Text = "Correct Answers: " + this.correctAnswersCount.ToString();
                }
                MarkCorrectAndIncorrectAnswers(submit.correctAnswerId);
                DisableAllButtons();
                this.answered = true;
            }
            catch(Exception err)
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                background_worker.CancelAsync();
            }
        }

        //mark the correct answer in green and the other in red
        private void MarkCorrectAndIncorrectAnswers(int correctAnswerId)
        {
            if (this.Answer1Key == correctAnswerId)
            {
                this.Answer1.Background = Brushes.Green;
                this.Answer2.Background = Brushes.Red;
                this.Answer3.Background = Brushes.Red;
                this.Answer4.Background = Brushes.Red;
            }
            else if (this.Answer2Key == correctAnswerId)
            {
                this.Answer1.Background = Brushes.Red;
                this.Answer2.Background = Brushes.Green;
                this.Answer3.Background = Brushes.Red;
                this.Answer4.Background = Brushes.Red;
            }
            else if (this.Answer3Key == correctAnswerId)
            {
                this.Answer1.Background = Brushes.Red;
                this.Answer2.Background = Brushes.Red;
                this.Answer3.Background = Brushes.Green;
                this.Answer4.Background = Brushes.Red;
            }
            else if (this.Answer4Key == correctAnswerId)
            {
                this.Answer1.Background = Brushes.Red;
                this.Answer2.Background = Brushes.Red;
                this.Answer3.Background = Brushes.Red;
                this.Answer4.Background = Brushes.Green;
            }
        }

        //new question - mark the buttons in purple
        private void ResetButtonsColors()
        {
            this.Answer1.Background = Brushes.Purple;
            this.Answer2.Background = Brushes.Purple;
            this.Answer3.Background = Brushes.Purple;
            this.Answer4.Background = Brushes.Purple;
        }

        //dont let the user to press the buttons
        private void DisableAllButtons()
        {

            this.Answer1.IsEnabled = false;
            this.Answer2.IsEnabled = false;
            this.Answer3.IsEnabled = false;
            this.Answer4.IsEnabled = false;
        }

        //let the user to press the buttons
        private void EnableAllButtons()
        {

            this.Answer1.IsEnabled = true;
            this.Answer2.IsEnabled = true;
            this.Answer3.IsEnabled = true;
            this.Answer4.IsEnabled = true;
        }
    }
}
