﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class StatisticsResponse
    {
        public int status;
        public string correctAnswers;
        public string totalAnswers;
        public string gamesNumber;
        public string averageTime;
        public string errMessage = "";

        public StatisticsResponse()
        {
            this.correctAnswers = "";
            this.averageTime = "";
            this.gamesNumber = "";
            this.totalAnswers = "";
        }

        public StatisticsResponse(string errMessage)
        {
            this.status = 0;
            this.correctAnswers = "";
            this.averageTime = "";
            this.gamesNumber = "";
            this.totalAnswers = "";
            this.errMessage = errMessage;
        }

        public StatisticsResponse(int status)
        {
            this.status = status;
            this.correctAnswers = "";
            this.averageTime = "";
            this.gamesNumber = "";
            this.totalAnswers = "";

        }

        public StatisticsResponse(int status, string correctAnswers,string totalAnswers, string gamesNumber, string averageTime)
        {
            this.status = status;
            this.correctAnswers = correctAnswers;
            this.averageTime = totalAnswers;
            this.gamesNumber = gamesNumber;
            this.totalAnswers = averageTime;
        }
    }
}
