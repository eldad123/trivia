﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class SubmitAnswerResponse
    {
        public int status;
        public int correctAnswerId;

        public SubmitAnswerResponse(int status, int correctAnswerId)
        {
            this.status = status;
            this.correctAnswerId = correctAnswerId;
        }
    }
}
