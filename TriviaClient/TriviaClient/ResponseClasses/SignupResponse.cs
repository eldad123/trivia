﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class SignupResponse
    {
        public int status;
        public string passwordError;
        public string usernameError;
        public string emailError;
        public string birthdayError;
        public string addressError;
        public string phoneError;
        public string errMessage = "";

        public SignupResponse(int status)
        {
            this.status = status;
            this.passwordError = "";
            this.usernameError = "";
            this.emailError = "";
            this.birthdayError = "";
            this.addressError = "";
            this.phoneError = "";
        }

        public SignupResponse(string errMessage)
        {
            this.status = 0;
            this.passwordError = "";
            this.usernameError = "";
            this.emailError = "";
            this.birthdayError = "";
            this.addressError = "";
            this.phoneError = "";
            this.errMessage = errMessage;
        }

        public SignupResponse(int status, string usernameError, string passwordError, string emailError, string phoneError, string addressError, string birthdayError)
        {
            this.status = status;
            this.passwordError = usernameError;
            this.usernameError = passwordError;
            this.emailError = emailError;
            this.phoneError = phoneError;
            this.birthdayError = birthdayError;
            this.addressError = addressError;
        }
    }
}
