﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriviaClient.classes;

namespace TriviaClient.ResponseClasses
{
    class LeaveGameResponse
    {
        public int status { get; set; }
        public string error { get; set; }

        [JsonConstructor]
        public LeaveGameResponse(int status)
        {
            this.status = status;
            this.error = "";
        }

        public LeaveGameResponse(string message)
        {
            this.status = Constants.FAILD_STATUS;
            this.error = message;
        }
    }
}
