﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriviaClient.classes;

namespace TriviaClient.ResponseClasses
{
    class BestScoresResponse
    {
        public int status;
        public string errMessage = "";
        public List<User> top5;

        public BestScoresResponse()
        {
            List<User> top5 = new List<User>();
        }

        public BestScoresResponse(int status)
        {
            this.status = status;
            List<User> top5 = new List<User>();
        }

        public BestScoresResponse(string errMessage)
        {
            this.status = 0;
            List<User> top5 = new List<User>();
            this.errMessage = errMessage;
        }

        public BestScoresResponse(int status, List<User> top5)
        {
            this.status = status;
            this.top5 = top5;
        }
    }
}
