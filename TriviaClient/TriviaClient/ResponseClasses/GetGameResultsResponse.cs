﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class GetGameResultsResponse
    {
        public int status;
        public List<PlayerResults> results;
        public string error = "";

        public GetGameResultsResponse(string error)
        {
            this.status = 0;
            this.results = new List<PlayerResults>();
            this.error = error;
        }


        public GetGameResultsResponse(int status, List<PlayerResults> results)
        {
            this.status = status;
            this.results = results;
        }
        public GetGameResultsResponse()
        {
            this.status = 0;
            this.results = new List<PlayerResults>();
        }
    }
}
