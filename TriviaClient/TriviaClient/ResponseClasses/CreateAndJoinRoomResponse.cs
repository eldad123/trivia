﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class CreateAndJoinRoomResponse
    {
        public int status;
        public string errMessage = "";

        public CreateAndJoinRoomResponse(int status)
        {
            this.status = status;
        }

        public CreateAndJoinRoomResponse(string errMessage)
        {
            this.status = 0;
            this.errMessage = errMessage;
        }

        public CreateAndJoinRoomResponse()
        {

        }
    }
}
