﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class LeaveRoomResponse
    {
        public int status;
        public string errMessage = "";

        public LeaveRoomResponse(string errMessage)
        {
            this.status = 0;
            this.errMessage = errMessage;
        }

        public LeaveRoomResponse(int status)
        {
            this.status = status;
        }
    }
}
