﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TriviaClient.DataClasses;

namespace TriviaClient.ResponseClasses
{
    class GetRoomsResponse
    {
        public List<RoomData> roomDataLst;
        public string err;
        public GetRoomsResponse()
        {
            roomDataLst = new List<RoomData>();
        }
        public GetRoomsResponse(List<RoomData> roomData)
        {
            this.roomDataLst = roomData;
        }

        public GetRoomsResponse(string err)
        {
            this.err = err;
        }
    }
}
