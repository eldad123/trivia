﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class PlayerResults
    {
        public string username { get; set; }
        public int correctAnswerCount { get; set; }
        public int wrongAnswerCount { get; set; }
        public double averageAnswerTime { get; set; }

        public PlayerResults(string username, int correctAnswerCount, int wrongAnswerCount, double averageAnswerTime)
        {
            this.username = username;
            this.correctAnswerCount = correctAnswerCount;
            this.averageAnswerTime = averageAnswerTime;
            this.wrongAnswerCount = wrongAnswerCount;
        }

        public double GetScore()
        {
            if(this.averageAnswerTime == 0)
            {
                return 0;
            }
            return this.correctAnswerCount / this.averageAnswerTime;
        }
    }
}
