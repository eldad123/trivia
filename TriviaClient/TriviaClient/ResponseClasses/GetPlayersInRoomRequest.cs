﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class GetPlayersInRoomResponse
    {
        public string players;
        public string err;
        public GetPlayersInRoomResponse()
        {
            
        }
        public GetPlayersInRoomResponse(string err)
        {
            this.err = err;
        }


    }
}
