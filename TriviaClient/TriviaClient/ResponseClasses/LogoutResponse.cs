﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class LogoutResponse
    {
        public int status;
        public string errMessage = "";

        public LogoutResponse(string errMessage)
        {
            this.status = 0;
            this.errMessage = errMessage;
        }

        public LogoutResponse(int status)
        {
            this.status = status;
        }
    }
}
