﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
     class SigninResponse
    {
        public int status;
        public string passwordError;
        public string usernameError;
        public string errMessage = "";

        public SigninResponse(int status)
        {
            this.status = status;
            this.passwordError = "";
            this.usernameError = "";
        }

        public SigninResponse(string errMessage)
        {
            this.status = 0;
            this.passwordError = "";
            this.usernameError = "";
            this.errMessage = errMessage;
        }

        public SigninResponse(int status, string usernameError ,string passwordError)
        {
            this.status = status;
            this.passwordError = usernameError;
            this.usernameError = passwordError;
        }
    }
}
