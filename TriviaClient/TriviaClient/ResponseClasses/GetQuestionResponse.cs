﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class GetQuestionResponse
    {
        public int status;
        public string question;
        private Dictionary<int, string> answers;
        public string error = "";

        public GetQuestionResponse(string error)
        {
            this.error = error;
        }

        public GetQuestionResponse(int status, string question, Dictionary<int, string> answers, Random rnd)
        {
            this.status = status;
            this.question = question;
            List<int> keys = this.ShuffleAnswersKeys(rnd, answers);

            this.answers = new Dictionary<int, string>();
            foreach (int key in keys)
            {
                this.answers.Add(key, answers[key]);
            }
        }

        /*
        suffle the answers
        input: random to grill numbers of shuffles, and the dictionary of answers ans their id
        output: list of answers id after shuffling
        */
        private List<int> ShuffleAnswersKeys(Random rnd, Dictionary<int, string> answers)
        {
            List<int> keys = answers.Keys.ToList<int>();
            int n = keys.Count;
            while (n > 1)
            {
                n--;
                int k = rnd.Next(answers.Count);
                int value = keys[k];
                keys[k] = keys[n];
                keys[n] = value;
            }
            return keys;
        }

        public Dictionary<int, string> GetAnswers()
        {
            return this.answers;
        }

    }
}
