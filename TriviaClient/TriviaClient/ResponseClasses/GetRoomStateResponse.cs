﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TriviaClient.ResponseClasses
{
    class GetRoomStateResponse
    {
        public int status;
        public bool hasGameBegun;
        public List<string> players;
        public int questionCount;
        public int answerTimeout;
        public string err = "";
        public GetRoomStateResponse()
        {

        }
        public GetRoomStateResponse(string err)
        {
            this.err = err;
        }
        public GetRoomStateResponse(int status, bool hasGameBegun, List<string> players,int questionCount,int answerTimeout)
        {
            this.status = status;
            this.hasGameBegun = hasGameBegun;
            this.players = players;
            this.questionCount = questionCount;
            this.answerTimeout = answerTimeout;
        }
            
    }
}
