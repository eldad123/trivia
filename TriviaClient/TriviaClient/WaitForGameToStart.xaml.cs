﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TriviaClient.classes;
using TriviaClient.ResponseClasses;

namespace TriviaClient
{
    /// <summary>
    /// Interaction logic for WaitForGameToStart.xaml
    /// </summary>
    public partial class WaitForGameToStart : Page
    {
        public string username;
        public string roomName;
        private int questionsCount;
        private int timePerQuestions;
        private BackgroundWorker background_worker = new BackgroundWorker();

        public WaitForGameToStart(string roomName, string username, int questionsCount, int timePerQuestions)
        {
            InitializeComponent();
            this.username = username;
            this.roomName = roomName;
            this.timePerQuestions = timePerQuestions;
            this.questionsCount = questionsCount;

            //initialize the background worker
            background_worker.WorkerSupportsCancellation = true;
            background_worker.WorkerReportsProgress = true;

            background_worker.DoWork += background_worker_DoWork;
            background_worker.ProgressChanged += background_worker_ProgressChanged;
            background_worker.RunWorkerCompleted += background_worker_RunWorkerCompleted;
            background_worker.RunWorkerAsync();
        }

        //Activate when the background worker is finished or canceled
        void background_worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
           
        }

        //Activate when reporting on proggress and init the users table or move to menu page
        void background_worker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            List<User> users = new List<User>();
            bool isFirst = true;
            try
            {
                //if the user has closed the room
                if (e.UserState is LeaveRoomResponse)
                {
                    background_worker.CancelAsync();
                    MessageBox.Show("The Admin has closed the room");
                    this.NavigationService.Navigate(new MenuPage(this.username));
                }
                else
                {
                    //init the users table if the game didn't start yet
                    if (!((GetRoomStateResponse)e.UserState).hasGameBegun)
                    {
                        foreach (string user in ((GetRoomStateResponse)e.UserState).players)
                        {
                            if (isFirst)
                            {
                                users.Add(new User(user + " ------- Admin"));
                                isFirst = false;
                            }
                            else
                            {
                                users.Add(new User(user));
                            }
                        }
                        dataGrid.ItemsSource = users;
                    }
                    else//if the game started
                    {
                        background_worker.CancelAsync();//cancel the background worker
                        this.NavigationService.Navigate(new GamePage(this.roomName, this.username, this.questionsCount, this.timePerQuestions));
                    }
                }
            }
            catch (Exception err)//if the server shut down
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
            }
        }

        //the thread that run in the background
        void background_worker_DoWork(object sender, DoWorkEventArgs e)
        {
            int i = 0;
            while (true)
            {
                //check if the thread was canceled
                if (background_worker.CancellationPending)
                {
                    e.Cancel = true;
                    break;

                }
                try
                {
                    //send get room state request and get responed
                    Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.GET_ROOM_STATE_CODE));
                    byte[] jsonResponseForRooms = Communicator.getMessageFromServer();
                    GetRoomStateResponse getState = Deserializer.DeserializerGetRoomState(jsonResponseForRooms);
                    
                    //if the request is not valid at this point
                    if (getState.err == "The message in not valid at this point")
                    {
                        throw new Exception("This option is invalid at this point");
                    }
                    else if (getState.status == Constants.FAILD_STATUS)//if the room was closed by the admin
                    {
                        //send leave room request and get responed
                        Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.LEAVE_ROOM_CODE));
                        jsonResponseForRooms = Communicator.getMessageFromServer();
                        LeaveRoomResponse leaveRoomResponse = Deserializer.DeserializerLeaveRoomResponse(jsonResponseForRooms);
                        //if the request is not valid at this point
                        if (leaveRoomResponse.errMessage == "The message in not valid at this point")
                        {
                            throw new Exception("This option is invalid at this point");
                        }
                        background_worker.ReportProgress(1, leaveRoomResponse);
                        Thread.Sleep(100);
                    }
                    else
                    {
                        //call reportProgress in order to init the users table
                        background_worker.ReportProgress(i, getState);
                        Thread.Sleep(100);
                    }
                }
                catch (Exception err)//if the server shut down
                {
                    MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                    background_worker.CancelAsync();//cancel the background worker
                }
                Thread.Sleep(3000);//wait for 3 seconds
                i++;
            }
        }

        private void Leave_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                background_worker.CancelAsync();//cancel the background worker

                //send leave room request and get responed
                Communicator.sendMessageToServer(Serializer.SerializerRequest(Constants.LEAVE_ROOM_CODE));
                byte[] jsonResponse = Communicator.getMessageFromServer();
                LeaveRoomResponse leaveRoom = Deserializer.DeserializerLeaveRoomResponse(jsonResponse);
                if (leaveRoom.errMessage == "The message in not valid at this point")
                {
                    throw new Exception("This option is invalid at this point");
                }
                this.NavigationService.Navigate(new MenuPage(this.username));
            }
            catch (Exception err)//if the server shutdown
            {
                MessageBox.Show("There was a server error please try to connect again\n Error Details:" + err.Message, "Error");
                background_worker.CancelAsync();//cancel background worker
            }

        }
    }
}
